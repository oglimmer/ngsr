define(["jquery", "atmosphere"], function($, atmosphere) {

	return function (globals) {
		"use strict";
	    var transport = 'websocket';
	    var subSocket = null;
	    var thiz = this;
	
	    var request = { 
	    	url: document.location.toString() + 'comm',
	        contentType : "application/json",
	        logLevel : 'info',
	        transport : transport,
	        trackMessageLength : true,
	        enableXDR: false, // default
	        reconnectInterval : 0, // default
	        timeout : 300000, // default
	        data: 'rnd='+ Math.random() // disables caching of dependent JS files
	    };
	    
	    this.push = function(data) {
	    	if(globals.deviceId != null) {
	    		data.deviceId=globals.deviceId;
	    	}
	    	console.log("SEND");
	    	console.log(data);
	    	subSocket.push(atmosphere.util.stringifyJSON(data));
	    };
	    
	    this.init = function() {
	    	subSocket = atmosphere.subscribe(request);
	    };
	
	    request.onOpen = function(response) {
	    	atmosphere.util.log("onOpen");
	    	atmosphere.util.log(response);
	        transport = response.transport;
	        // Carry the UUID. This is required if you want to call subscribe(request) again.
	        request.uuid = response.request.uuid;
	        thiz.push({
	        	cmd:'init',
	        	source:'Base'
	        });
	    };
	
	    request.onClientTimeout = function(r) {
	    	atmosphere.util.log("onClientTimeout");
	        setTimeout(function (){
	            subSocket = atmosphere.subscribe(request);
	        }, request.reconnectInterval);
	    };
	
	    request.onReopen = function(response) {
	    	atmosphere.util.log("onReopen");
	    };
	
	    // For demonstration of how you can customize the fallbackTransport using the onTransportFailure function
	    request.onTransportFailure = function(errorMsg, request) {
	    	atmosphere.util.log("onTransportFailure");
	        atmosphere.util.info(errorMsg);
	        request.fallbackTransport = "long-polling";
	    };
	    
	    function callHandler(methodName, json) {
	    	if(json[methodName]!=='undefined') {
    			globals.messageHandler[methodName](json[methodName]);
	    	}
	    }
	
	    request.onMessage = function (response) {
	    	var message = response.responseBody;
	    	var json = null;
	        try {
	            json = atmosphere.util.parseJSON(message);     
	        } catch (e) {
	        	atmosphere.util.log('This doesn\'t look like a valid JSON: ', message);
	        	return;
	        }
	        console.log("RECV");
	        console.log(json);
	        callHandler("deviceId", json);
	        callHandler("playTab", json); // build play tab
	        callHandler("vars", json); // change innerHTML for any tag
	        callHandler("htmlAttrs", json); // change attributes for any tag
	        callHandler("msg", json); // add a msg (info/warning/error)

	        callHandler("model", json);
	    };
	
	    request.onClose = function(response) {
	    	atmosphere.util.log("onClose");
	        if (subSocket!=null) {
	        	// send close msg
	        }
	    };
	
	    request.onError = function(response) {
	    	atmosphere.util.log("onError");
	    };
	
	    request.onReconnect = function(request, response) {
	    	atmosphere.util.log("onReconnect");
	    };
	
	};
	
});
