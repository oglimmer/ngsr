require.config({
	urlArgs: "bust=" + (new Date()).getTime(),
	shim: {
		'atmosphere': {
			exports: 'atmosphere'
		}
	},
    // Add this map config in addition to any baseUrl or
    // paths config you may already have in the project.	
    map: {
      // '*' means all modules will get 'jquery-private'
      // for their 'jquery' dependency.
      '*': { 'jquery': 'jquery-private' },

      // 'jquery-private' wants the real jQuery module
      // though. If this line was not here, there would
      // be an unresolvable cyclic dependency.
      'jquery-private': { 'jquery': 'jquery' }
    }
});

require(["cl_servercommunication", "cl_messagehandler", "in_menuSelect", "in_messageTab"], function(Servercommunication, Messagehandler) {
	var globals = {};
	globals.deviceId = localStorage.getItem("deviceId");
	globals.serverComm = new Servercommunication(globals);
	globals.messageHandler = new Messagehandler(globals);
	globals.serverComm.init();	
});
