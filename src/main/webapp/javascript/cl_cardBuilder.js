define(["jquery"], function($) {

	return function(globals) {
		
		function addId(obj, postFix) {
			obj.attr('id', obj.attr('id') + postFix);	
		}

		function setHtmlOnTag(tag, htmlText) {			
			tag.html(htmlText);
		}

		function processChild(tag, cardData) {			
			var idAttr = tag.attr("id");
			if(typeof(idAttr) !== 'undefined') {	
				if(idAttr == "buttonPane"){
					if(typeof(cardData.actions)!=='undefined') {
						$.each(cardData.actions, function() {
							var newInput = $("<input />", {
								id:idAttr+cardData.cardId+this.name,
								type:this.type,
								name:this.name,
								value:"true"
							});
							tag.append(newInput);
							var newLabel = $("<label />", {
								'for':idAttr+cardData.cardId+this.name,
								text:this.label
							});
							tag.append(newLabel);
						});
					}
				} else {
					setHtmlOnTag(tag, cardData[idAttr]);
					addId(tag, cardData.cardId);
				}
			}	
			
		}
		
		function construct(cardData, templateId) {			
			var baseDiv = $("#"+templateId+"Card");
			var cardDiv = baseDiv.clone(true);
			addId(cardDiv, cardData.cardId);
			cardDiv.css("display", "");			
			$(cardDiv).find("*").each(function() {				
				processChild($(this), cardData);				
			});			
			return cardDiv;
		}
		
		this.build = function(cardData) {
			return construct(cardData, cardData.clazz);
		};
	
	};
	
});