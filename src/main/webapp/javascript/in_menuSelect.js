define(["jquery"], function($) {

	return new function () {
		this.selected = "menuTabPlay";
		var thiz = this;
		var originalBGColor = $("#menuTabPlay").css("background-color");
		$("#menuTabPlay").click(function() {
			$("#playTab").show();
			$("#infoTab").hide();
			$("#cardTab").hide();
			$("#menuTabPlay").css("border", "1px solid black");
			$("#menuTabPlay").css("background-color", originalBGColor);
			$("#menuTabInfo").css("border", "0px");
			$("#menuTabCard").css("border", "0px");
			thiz.selected = "menuTabPlay";
		});
		$("#menuTabInfo").click(function() {
			$("#playTab").hide();
			$("#infoTab").show();
			$("#cardTab").hide();		
			$("#menuTabPlay").css("border", "0px");
			$("#menuTabInfo").css("border", "1px solid black");
			$("#menuTabCard").css("border", "0px");
			thiz.selected = "menuTabInfo";
		});
		$("#menuTabCard").click(function() {
			$("#playTab").hide();
			$("#infoTab").hide();
			$("#cardTab").show();		
			$("#menuTabPlay").css("border", "0px");
			$("#menuTabInfo").css("border", "0px");
			$("#menuTabCard").css("border", "1px solid black");
			thiz.selected = "menuTabCard";
		});
		this.htmlRefreshed = function() {
			if(this.selected != "menuTabPlay") {
				$("#menuTabPlay").css("background-color", "green");
			}
		};
	};
	
});
