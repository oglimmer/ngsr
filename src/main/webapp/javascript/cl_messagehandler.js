define(["jquery", "fn_blink", "in_menuSelect", "cl_cardBuilder"], function($, blink, menuSelect, CardBuilder) {

	return function(globals) {
		
		var cardBuilder = new CardBuilder(globals);
		
		var thiz = this;
		// helper
		
		function setHtml(id, html) {
			$('#'+id).html($(html));
		}
		function enableClickable(clickDisabled) {
			$(".clickable").each(function() {
				if(clickDisabled !== true) {
	    			$(this).click(function() {
	    				blink(this, globals.serverComm);
	    			});
				} else {
//					$(this).css("text-decoration","line-through");
					$(this).css("background","url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAADCAYAAABWKLW/AAAAEklEQVQIHWNgYGD4D8QoAFMAADXxAv6fd9g0AAAAAElFTkSuQmCC) repeat");
				}
    		});
		}
		function htmlAttr(value) {
			var htmlId = value.id;
			var cssName = value.css;
			var cssValue = value.value;
			$("#"+htmlId).css(cssName, cssValue);
		};
		
		function setDeviceIdDiv() {
			if($("#deviceIdDiv")) {
    			$("#deviceIdDiv").html("DeviceId: "+globals.deviceId);
    		}
		}
		
		function html(value, tabId) {
			if(typeof(value)!=='undefined') {						
				menuSelect.htmlRefreshed();
				setHtml(tabId, value.html);
				thiz.vars(value.vars);
				thiz.htmlAttrs(value.htmlAttrs);
				buildCards(value.cards, tabId+"_cards");
				enableClickable(value.clickDisabled);
				
				if(typeof(value.actions)!=='undefined') {		
					$.each(value.actions, function() {
						var newInput = $("<input />", {			
							id:this.cmd,
							type:this.type,
							click:blink(this, globals.serverComm),
							value:this.label
						});
						$("#cardTab_actions").append(newInput);
					});
				}
				setDeviceIdDiv();
			}
		};
		
		function buildCard(cardData, targetDiv) {
			var cardDiv = cardBuilder.build(cardData);
			if($("#"+cardDiv.attr("id")).length == 0) {
				$("#"+targetDiv).append(cardDiv);
			} else {
				//TODO update
			}
		}
		
		function buildCards(cards, targetDiv) {			
			if(typeof(cards)!=='undefined') {
				$.each(cards, function() {
					buildCard(this, targetDiv);
				});
			}
		}
		
		function changeVar(key, val) {
			var htmlObj = $("#"+key);
			// check for existence
			if(htmlObj.length != 0) {
    			if(htmlObj.prop("tagName") == "INPUT") {
    				htmlObj.val(val);
    			} else {
    				htmlObj.html(val);
    			}
			}
		}
		
		// message handlers
		
		/**
		 * root key = "vars"
		 * nested key = "vars"
		 * supports value = object (as many key-value)
		 */
		this.vars = function(value) {
			if(typeof(value)!=='undefined') {
        		$.each(value, function( key, val ) {
        			changeVar(key,val);
        		});
    		}
		};
		
		/**
		 * root key = "htmlAttrs"
		 * nested key = "htmlAttrs"
		 * supports value = object or array of {id:string, css:string, value:string}
		 */
		this.htmlAttrs = function(value) {			
			if(value instanceof Array) {
				$.each(value, function(ind, ele) {
					htmlAttr(ele);
				});
			} else if(typeof(value)!=='undefined'){
				htmlAttr(value);
			}
		};
		
		/**
		 * root key = "playTab"
		 * supports value = {html:string, vars:array_object, htmlAttrs:array_object, clickDisabled:boolean, cards:array_object}
		 */
		this.playTab = function(value) {			
			html(value, 'playTab');
		};
		
		/**
		 * root key = "deviceId"
		 * supports value = string
		 */
		this.deviceId = function(value) {
			if(typeof(value)!=='undefined') {
				globals.deviceId = value;
				localStorage.setItem("deviceId", value);
			}
		};
		
		/**
		 * root key = "msg"
		 * supports value = array of {text:string}
		 */
		this.msg = function(value) {
			if(typeof(value)!=='undefined') {
				$.each(value, function(ind, ele) {
					var infoDiv = $("<div/>");
					infoDiv.html(ele.text);
					if(ele.level == 'fatal') {
						infoDiv.css("background-color", "red");
					} else if(ele.level == 'error') {
						infoDiv.css("background-color", "yellow");
					}
					$("#msgTab").append(infoDiv);
				});
			}
		};
		
		this.model = function(value) {
			if(typeof(value)!=='undefined') {
				$.each(value, function(key, val) {
					if(key == 'cards') {
						$.each(val, function() {
							if(this.DELETED) {
								$("#"+this.id).remove();
							} else {
								buildCard(this, 'cardTab_cards');
							}
						});
					} else {
						changeVar(key,val);
					}
				});
			}
		};
		
	};
	
});
