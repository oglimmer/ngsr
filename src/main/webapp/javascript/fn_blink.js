define(["jquery"], function($) {
	
	var lastInvocation = {
		timestamp: 0,
		id: ''
	};

	return function blinkAndSend(id, comm) {
		var obj = typeof(id)==='string' ? $("#"+id) : $(id);
		
		var now = new Date().getTime();
		if(lastInvocation.timestamp > now-100 && obj.attr("id") == lastInvocation.id) {
			return;
		}
		lastInvocation.timestamp = now;
		lastInvocation.id = obj.attr("id");
		
		var originalBackGroundColor = obj.css("background-color");
		obj.css("background-color", "red");
		setTimeout(function () {
			obj.css("background-color", originalBackGroundColor);
		}, 100);	
		var paramData = obj.attr("id").split("_");
		var additionalParams = {};
		$("input").each(function() {
			var ele = $(this);
			additionalParams[ele.attr("name")] = ele.val();
		});
		console.log("button "+lastInvocation.id+" was clicked at "+lastInvocation.timestamp);
		comm.push({
			source: paramData[0],
			cmd: "buttonClick", 
			param: paramData[1],
			additionalParams : additionalParams
		});	
	};

});
