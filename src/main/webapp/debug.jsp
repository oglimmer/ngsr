<!DOCTYPE html>
<%@page import="de.oglimmer.ngsr.logic.GameManager"%>
<%@page import="de.oglimmer.ngsr.state.State"%>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>NGSR</title>
<link rel="stylesheet" href="css/styles.css" />
</head>
<body>
	
	<ol>
<%
	for(State state : GameManager.INSTANCE.getGame().getStates()) {
%>
		<li><%=state%></li>
<%		
	}
%>
	</ol>

<script>
setTimeout(function() {
	location.reload();
},1000);
</script>	
	
</body>
</html>
