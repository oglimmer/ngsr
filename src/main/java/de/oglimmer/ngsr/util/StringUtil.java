package de.oglimmer.ngsr.util;

import org.apache.commons.lang3.StringUtils;

public class StringUtil {

	public static String makeReadable(String str) {
		String[] parts = str.split("_");
		StringBuilder buff = new StringBuilder();
		for (String s : parts) {
			if (buff.length() > 0) {
				buff.append('-');
			}
			s = s.toLowerCase();
			s = StringUtils.capitalize(s);
			buff.append(s);
		}
		return buff.toString();
	}

}
