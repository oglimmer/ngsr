package de.oglimmer.ngsr.logic;

import lombok.Data;
import net.sf.json.JSONObject;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

@Data
public class ClientSideModel {

	private Player player;

	private JSONBuilder base;

	public ClientSideModel(Player player) {
		this.player = player;
		base = player.getJSON();
	}

	public void reset() {
		base = player.getJSON();
	}

	public JSONObject toJSON() {
		JSONBuilder latest = player.getJSON();
		if (base == null) {
			return latest.build();
		}
		return new JSONDelta().calcDelta(base, latest);
	}

	public void setFull() {
		base = null;
	}

}
