package de.oglimmer.ngsr.logic;

import org.atmosphere.cpr.AtmosphereResource;

public interface InterfaceDevice {

	String getId();

	AtmosphereResource getResource();

	void setResource(AtmosphereResource resource);

}
