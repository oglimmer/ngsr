package de.oglimmer.ngsr.logic;

import java.util.HashSet;
import java.util.Set;

import lombok.Data;
import de.oglimmer.ngsr.card.Card;
import de.oglimmer.ngsr.web.atomsphere.JSONArrayBuilder;

@Data
public class PlayerCards {

	private Set<Card> cards = new HashSet<>();

	public void playOnTable(Card baseCard) {
		cards.add(baseCard);
	}

	public JSONArrayBuilder getJSON() {
		JSONArrayBuilder builder = JSONArrayBuilder.newArray();
		for (Card card : cards) {
			builder.add(card.getJSON());
		}
		return builder;
	}

}
