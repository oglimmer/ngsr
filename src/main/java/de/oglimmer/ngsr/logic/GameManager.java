package de.oglimmer.ngsr.logic;

import lombok.Getter;

public enum GameManager {
	INSTANCE;

	@Getter
	private Game game;

	public Game createGame(int playerNo, int reputationToWin, int numberOfObjectives) {
		game = new Game(playerNo, reputationToWin, numberOfObjectives);
		return game;
	}

}
