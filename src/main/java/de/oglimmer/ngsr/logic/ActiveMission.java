package de.oglimmer.ngsr.logic;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.RandomStringUtils;
import org.atmosphere.cpr.AtmosphereResource;

import de.oglimmer.ngsr.card.CardManager;
import de.oglimmer.ngsr.card.Challenge;
import de.oglimmer.ngsr.card.Objective;

@Slf4j
@Data
@ToString(exclude = "resource")
public class ActiveMission implements InterfaceDevice {

	private String id;
	private AtmosphereResource resource;
	private boolean assigned;

	private Objective objective;
	private List<Challenge> challenges = new ArrayList<Challenge>();

	public ActiveMission() {
		this.id = RandomStringUtils.random(3, true, false);
		log.debug("Create ActiveMission {}", id);
		objective = CardManager.INSTANCE.getObjective();
		for (int i = 0; i < 4; i++) {
			challenges.add(CardManager.INSTANCE.getChallenge());
		}
	}

}
