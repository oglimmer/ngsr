package de.oglimmer.ngsr.logic;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.RandomStringUtils;
import org.atmosphere.cpr.AtmosphereResource;

import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

@Slf4j
@Data
@ToString(exclude = { "game", "resource" })
@EqualsAndHashCode(of = "id")
public class Player implements InterfaceDevice {

	private Game game;

	private String name;
	private int cash;
	private int reputation;
	private PlayerCards playerCards = new PlayerCards();

	private String id;
	private AtmosphereResource resource;

	private ClientSideModel model = new ClientSideModel(this);

	public Player(AtmosphereResource resource, Game game) {
		this.game = game;
		this.id = RandomStringUtils.random(3, true, false);
		this.resource = resource;
		this.cash = 4;
		log.debug("Create Player {}", id);
	}

	public boolean isCurrentPlayer() {
		return getGame().getCurrentPlayer() == this;
	}

	public void newTurn() {
		cash += 4;
	}

	public void payCash(int cost) {
		cash -= cost;
	}

	public JSONBuilder getJSON() {
		JSONBuilder builder = JSONBuilder.newObject();
		builder.attribute("cash", cash);
		builder.attribute("reputation", reputation);
		builder.attribute("cards", playerCards.getJSON());
		return builder;
	}

}
