package de.oglimmer.ngsr.logic;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import de.oglimmer.ngsr.state.RootGameState;
import de.oglimmer.ngsr.state.State;
import de.oglimmer.ngsr.state.WaitingForOthersToJoinState;

@Slf4j
@ToString
public class Game {

	private int reputationToWin;
	private int numberOfObjectives;

	@Getter
	private Map<String, Player> players = new HashMap<>();
	private LinkedList<Player> playQueue = new LinkedList<Player>();
	@Getter
	private Map<String, ActiveMission> activeMissions = new HashMap<>();
	@Getter
	private LinkedList<State> states = new LinkedList<>();

	public Game(int playerNo, int reputationToWin, int numberOfObjectives) {
		this.reputationToWin = numberOfObjectives;
		this.numberOfObjectives = numberOfObjectives;
		pushState(new WaitingForOthersToJoinState(this, playerNo));
	}

	public void createActiveMissions() {
		for (int i = 0; i < numberOfObjectives; i++) {
			ActiveMission am = new ActiveMission();
			activeMissions.put(am.getId(), am);
		}
	}

	public Player createPlayer(InterfaceDevice ad) {
		Player newPlayer = new Player(ad.getResource(), this);
		players.put(newPlayer.getId(), newPlayer);
		return newPlayer;
	}

	public ActiveMission getUnassignedActiveMission(AnonymousDevice ad) {
		for (ActiveMission am : activeMissions.values()) {
			if (!am.isAssigned()) {
				am.setAssigned(true);
				am.setResource(ad.getResource());
				return am;
			}
		}
		assert false;
		return null;
	}

	public InterfaceDevice getInterfaceDevice(String id) {
		InterfaceDevice intDev = players.get(id);
		if (intDev == null) {
			intDev = activeMissions.get(id);
		}
		return intDev;
	}

	public void nextPlayer() {
		Player lastPlayer = playQueue.removeFirst();
		if (playQueue.isEmpty()) {
			resetPlayQueue();
		}
		Player currentPlayer = playQueue.getFirst();
		currentPlayer.newTurn();
		getCurrentState().updateUI(currentPlayer);
		getCurrentState().updateUI(lastPlayer);
	}

	public State getCurrentState() {
		return states.getFirst();
	}

	public void startGame() {
		cancelState();
		resetPlayQueue();
		playQueue.getFirst().newTurn();
		pushState(new RootGameState(this));
		getCurrentState().initUI();
	}

	private void resetPlayQueue() {
		playQueue.addAll(players.values());
		Collections.shuffle(playQueue);
	}

	public void pushState(State newState) {
		states.push(newState);
		newState.enter();
	}

	public Player getCurrentPlayer() {
		return playQueue.getFirst();
	}

	public void cancelState() {
		State state = states.pop();
		state.leave();
	}

	public void resetAll() {
		for (Player otherPlayer : getPlayers().values()) {
			otherPlayer.getModel().reset();
		}
	}
}
