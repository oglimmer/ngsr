package de.oglimmer.ngsr.logic;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import org.atmosphere.cpr.AtmosphereResource;

@Slf4j
@Data
@ToString(exclude = "resource")
public class AnonymousDevice implements InterfaceDevice {

	private String id;
	private AtmosphereResource resource;

	public AnonymousDevice(AtmosphereResource resource) {
		this.id = "anonymous";
		this.resource = resource;
		log.debug("Create AnonymousDevice {}", id);
	}

}
