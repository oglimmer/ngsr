package de.oglimmer.ngsr.logic;

import java.util.Iterator;

import lombok.SneakyThrows;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

public class JSONDelta {

	public JSONObject calcDelta(JSONBuilder base, JSONBuilder latest) {
		return calcDelta(base.build(), latest.build());
	}

	/**
	 * Compares two json objects (all the flat and nested values from all attributes) and return those which are
	 * different in the return value. In arrays which contain elements of JSONObjects removed elements are still
	 * includes as {id:ID, DELETED:true}, however arrays of strings/numbers/booleans are just completely replaced.
	 * 
	 * @param baseJson
	 *            origin object
	 * @param latestJson
	 *            latest - potentially changed - object
	 * @return holding all the differences
	 */
	@SneakyThrows(value = JSONException.class)
	public JSONObject calcDelta(JSONObject baseJson, JSONObject latestJson) {
		JSONObject difference = new JSONObject();
		for (@SuppressWarnings("unchecked")
		Iterator<String> it = baseJson.keys(); it.hasNext();) {
			String key = it.next();
			new JSONObjectAttribute(baseJson, latestJson, difference, key).calcDelta();
		}
		if (difference.size() > 0 && baseJson.has("id")) {
			difference.put("id", baseJson.get("id"));
		}
		return difference;
	}

	class JSONObjectAttribute {

		private Object baseVal;
		private Object latestVal;
		private boolean hasAttr;
		private JSONObject parentResult;
		private String attrName;

		public JSONObjectAttribute(JSONObject baseJson, JSONObject latestJson, JSONObject parentResult, String attrName)
				throws JSONException {
			this.baseVal = baseJson.get(attrName);
			this.latestVal = latestJson.get(attrName);
			this.parentResult = parentResult;
			this.attrName = attrName;
			this.hasAttr = latestJson.has(attrName);
		}

		void calcDelta() throws JSONException {
			if (hasAttr) {
				calcDeltaExisting();
			} else {
				// attribute was removed
				assert false;
			}
		}

		private void calcDeltaExisting() throws JSONException {
			if (baseVal instanceof JSONObject) {
				JSON obj = new JSON(parentResult, attrName, baseVal, latestVal);
				obj.checkModifiedObject();
			} else if (baseVal instanceof JSONArray) {
				ArrayFacade array = new ArrayFacade(parentResult, attrName, baseVal, latestVal);
				array.calcArrayDelta();
			} else {
				checkForModifiedAttribute();
			}
		}

		private void checkForModifiedAttribute() throws JSONException {
			if (!baseVal.equals(latestVal)) {
				parentResult.put(attrName, latestVal);
			}
		}

	}

	class JSON {

		private JSONObject parentResult;
		private String attrName;
		private JSONObject baseJson;
		private JSONObject latestJson;

		public JSON(JSONObject parentResult, String attrName, Object baseVal, Object latestVal) {
			this.parentResult = parentResult;
			this.attrName = attrName;
			this.baseJson = (JSONObject) baseVal;
			this.latestJson = (JSONObject) latestVal;
		}

		private void checkModifiedObject() throws JSONException {
			JSONObject jsonObj = JSONDelta.this.calcDelta(baseJson, latestJson);
			if (jsonObj.size() > 0) {
				parentResult.put(attrName, latestJson);
			}
		}

	}

	class ArrayFacade {

		private JSONObject parentResult;
		private String parentAttrName;
		private JSONArray baseArray;
		private JSONArray latestArray;

		public ArrayFacade(JSONObject parentResult, String parentAttrName, Object baseVal, Object latestVal) {
			this.parentResult = parentResult;
			this.parentAttrName = parentAttrName;
			this.baseArray = (JSONArray) baseVal;
			this.latestArray = (JSONArray) latestVal;
		}

		public void calcArrayDelta() throws JSONException {
			Object firstElement = null;
			if (baseArray.size() > 0) {
				firstElement = baseArray.get(0);
			} else if (latestArray.size() > 0) {
				firstElement = latestArray.get(0);
			}
			if (firstElement != null) {
				if (firstElement instanceof JSONObject) {
					new ArrayJson(parentResult, parentAttrName, baseArray, latestArray).calcArrayDelta();
				} else {
					new ArrayObject(parentResult, parentAttrName, baseArray, latestArray).calcArrayDelta();
				}
			}
		}

	}

	class ArrayObject {

		private JSONObject parentResult;
		private String parentAttrName;
		private JSONArray baseArray;
		private JSONArray latestArray;

		public ArrayObject(JSONObject parentResult, String parentAttrName, Object baseVal, Object latestVal) {
			this.parentResult = parentResult;
			this.parentAttrName = parentAttrName;
			this.baseArray = (JSONArray) baseVal;
			this.latestArray = (JSONArray) latestVal;
		}

		void calcArrayDelta() throws JSONException {
			if (baseArray.size() != latestArray.size()) {
				update();
			} else {
				for (int i = 0; i < baseArray.size(); i++) {
					if (!baseArray.get(i).equals(latestArray.get(i))) {
						update();
						break;
					}
				}
			}
		}

		private void update() {
			parentResult.put(parentAttrName, latestArray);
		}
	}

	class ArrayJson {

		private JSONObject parentResult;
		private String parentAttrName;
		private JSONArray baseArray;
		private JSONArray latestArray;
		private JSONArray deltaArray = new JSONArray();

		public ArrayJson(JSONObject parentResult, String parentAttrName, Object baseVal, Object latestVal) {
			this.parentResult = parentResult;
			this.parentAttrName = parentAttrName;
			this.baseArray = (JSONArray) baseVal;
			this.latestArray = (JSONArray) latestVal;
		}

		void calcArrayDelta() throws JSONException {
			checkForModifiedAndDeleted();
			checkForAdded();
		}

		private void checkForAdded() throws JSONException {
			for (int i = 0; i < latestArray.size(); i++) {
				checkForAddedAttribute(i);
			}
			if (deltaArray.size() > 0) {
				parentResult.put(parentAttrName, deltaArray);
			}
		}

		private void checkForAddedAttribute(int ind) throws JSONException {
			JSONObject latestObj = latestArray.getJSONObject(ind);
			String id = latestObj.getString("id");
			JSONObject baseObj = findObjectById(baseArray, id);

			if (baseObj == null) {
				deltaArray.add(latestObj);
			}
		}

		private void checkForModifiedAndDeleted() throws JSONException {
			JSONArray differenceArray = new JSONArray();
			for (int i = 0; i < baseArray.size(); i++) {
				checkForModifiedAndDeletedAttribute(i);
			}

			if (differenceArray.size() > 0) {
				parentResult.put(parentAttrName, differenceArray);
			}
		}

		private void checkForModifiedAndDeletedAttribute(int ind) throws JSONException {
			JSONObject baseObj = (JSONObject) baseArray.get(ind);
			String id = baseObj.getString("id");

			JSONObject latestObj = findObjectById(latestArray, id);
			if (latestObj == null) {
				setAsDeletedItem(id);
			} else {
				checkModifiedArrayElement(baseObj, latestObj);
			}
		}

		private void checkModifiedArrayElement(JSONObject baseVal, JSONObject latestVal) {
			JSONObject deltaObj = JSONDelta.this.calcDelta(baseVal, latestVal);
			if (deltaObj.size() > 0) {
				deltaArray.add(deltaObj);
			}
		}

		private void setAsDeletedItem(String id) throws JSONException {
			JSONObject latestObj = new JSONObject();
			latestObj.put("id", id);
			latestObj.put("DELETED", true);
			deltaArray.add(latestObj);
		}

		private JSONObject findObjectById(JSONArray arrayToSearch, String id) throws JSONException {
			for (int i = 0; i < arrayToSearch.size(); i++) {
				JSONObject obj = arrayToSearch.getJSONObject(i);
				if (obj.getString("id").equals(id)) {
					return obj;
				}
			}
			return null;
		}

	}

}
