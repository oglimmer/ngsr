package de.oglimmer.ngsr.logic;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

@Slf4j
public enum Answer {
	INSTANCE;

	@Getter
	private ThreadLocal<Map<InterfaceDevice, JSONBuilder>> responseData = new ThreadLocal<>();

	public JSONBuilder get(InterfaceDevice id) {
		JSONBuilder build = responseData.get().get(id);
		if (build == null) {
			build = JSONBuilder.newObject();
			responseData.get().put(id, build);
		}
		return build;
	}

	public void reset() {
		responseData.set(new HashMap<InterfaceDevice, JSONBuilder>());
	}

	public void send() {
		for (InterfaceDevice id : responseData.get().keySet()) {
			send(id, responseData.get().get(id).build());
		}
	}

	private void send(InterfaceDevice requestingDevice, JSONObject data) {
		if (data.size() > 0 && requestingDevice.getResource() != null) {
			debugOut(data);
			requestingDevice.getResource().getBroadcaster().broadcast(data.toString(), requestingDevice.getResource());
		}
	}

	private void debugOut(JSONObject data) {
		try {
			log.trace(data.toString(2));
		} catch (JSONException e) {
			log.error("Failed to debug out json", e);
		}
	}
}
