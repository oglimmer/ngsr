package de.oglimmer.ngsr.state;

import static de.oglimmer.ngsr.web.atomsphere.JSONBuilder.newObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.Player;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;
import de.oglimmer.ngsr.web.controller.ControllerFactory;
import de.oglimmer.ngsr.web.controller.EnterPlayernameController;

@Data
@EqualsAndHashCode(callSuper = false)
public class WaitingForOthersToJoinState extends AbstractState {

	private int playerNo;

	public WaitingForOthersToJoinState(Game game, int playerNo) {
		super(game);
		this.playerNo = playerNo;
	}

	@Override
	protected JSONBuilder getUpdateUIPlayTabForPlayer(Player requestingDevice) {
		if (requestingDevice.getName() == null) {
			return ControllerFactory.INSTANCE.getController(EnterPlayernameController.NAME).getUpdateUIPlayTab(
					requestingDevice);
		}
		return newObject().attribute("playTab", newObject().resourceAttribute("html", "waitingForOthersToJoin.html"));
	}

	public boolean playerAdded() {
		int playersWithName = 0;
		for (Player player : getGame().getPlayers().values()) {
			if (player.getName() != null) {
				playersWithName++;
			}
		}
		if (playerNo == playersWithName) {
			getGame().startGame();
			return true;
		}
		return false;
	}

}
