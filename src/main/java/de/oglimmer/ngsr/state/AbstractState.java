package de.oglimmer.ngsr.state;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.ToString;
import de.oglimmer.ngsr.logic.ActiveMission;
import de.oglimmer.ngsr.logic.AnonymousDevice;
import de.oglimmer.ngsr.logic.Answer;
import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.InterfaceDevice;
import de.oglimmer.ngsr.logic.Player;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;
import de.oglimmer.ngsr.web.controller.ControllerFactory;
import de.oglimmer.ngsr.web.controller.GameTabController;
import de.oglimmer.ngsr.web.controller.PortalController;

@Data
@ToString(exclude = "game")
public class AbstractState implements State {

	@Setter(AccessLevel.PRIVATE)
	private Game game;

	public AbstractState(Game game) {
		this.game = game;
	}

	@Override
	public void updateUI() {
		for (Player player : game.getPlayers().values()) {
			updateUI(player);
		}
		for (ActiveMission am : game.getActiveMissions().values()) {
			updateUI(am);
		}
	}

	@Override
	public void updateUI(InterfaceDevice requestingDevice) {
		updateUIPlayTab(requestingDevice);
	}

	private void updateUIPlayTab(InterfaceDevice requestingDevice) {
		JSONBuilder playTabData = null;
		if (requestingDevice instanceof Player) {
			playTabData = getUpdateUIPlayTabForPlayer((Player) requestingDevice);
		} else if (requestingDevice instanceof ActiveMission) {
			playTabData = getUpdateUIPlayTabForMission((ActiveMission) requestingDevice);
		} else if (requestingDevice instanceof AnonymousDevice) {
			playTabData = getUpdateUIPlayTabForAnonymous(requestingDevice);
		}
		if (playTabData != null) {
			Answer.INSTANCE.get(requestingDevice).copy(playTabData);
		}
	}

	protected JSONBuilder getUpdateUIPlayTabForPlayer(Player requestingDevice) {
		return null;
	}

	protected JSONBuilder getUpdateUIPlayTabForMission(ActiveMission requestingDevice) {
		return ControllerFactory.INSTANCE.getController(GameTabController.NAME).getUpdateUIPlayTab(requestingDevice);
	}

	protected JSONBuilder getUpdateUIPlayTabForAnonymous(InterfaceDevice requestingDevice) {
		return ControllerFactory.INSTANCE.getController(PortalController.NAME).getUpdateUIPlayTab(requestingDevice);
	}

	/**
	 * Called when a user-agent initially builds its UI
	 * 
	 * @param state
	 * @param requestingDevice
	 */
	@Override
	public void initUI(InterfaceDevice requestingDevice) {
		updateUI(requestingDevice);
		if (requestingDevice instanceof Player) {
			((Player) requestingDevice).getModel().setFull();
		}
	}

	@Override
	public void initUI() {
		for (Player player : game.getPlayers().values()) {
			initUI(player);
		}
		for (ActiveMission am : game.getActiveMissions().values()) {
			initUI(am);
		}
	}

	@Override
	public void enter() {

	}

	@Override
	public void leave() {

	}
}
