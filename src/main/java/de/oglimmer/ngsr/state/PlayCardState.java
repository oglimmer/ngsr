package de.oglimmer.ngsr.state;

import lombok.Data;
import lombok.EqualsAndHashCode;
import de.oglimmer.ngsr.card.Card;
import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.InterfaceDevice;
import de.oglimmer.ngsr.logic.Player;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;
import de.oglimmer.ngsr.web.controller.ControllerFactory;
import de.oglimmer.ngsr.web.controller.GamePhoController;
import de.oglimmer.ngsr.web.controller.PlayCardCheckController;
import de.oglimmer.ngsr.web.controller.PlayCardController;

@Data
@EqualsAndHashCode(callSuper = false)
public class PlayCardState extends AbstractState {

	private Player requestingPlayer;

	private Card card;

	public PlayCardState(Game game, Player requestingPlayer) {
		super(game);
		this.requestingPlayer = requestingPlayer;
	}

	@Override
	protected JSONBuilder getUpdateUIPlayTabForPlayer(Player requestingDevice) {
		String controllerName = getControllerName(requestingDevice);
		JSONBuilder data = ControllerFactory.INSTANCE.getController(controllerName)
				.getUpdateUIPlayTab(requestingDevice);
		addClickAllowed(requestingDevice, data);
		return data;
	}

	private String getControllerName(InterfaceDevice requestingDevice) {
		if (requestingPlayer == requestingDevice) {
			if (card == null) {
				return PlayCardController.NAME;
			} else {
				return PlayCardCheckController.NAME;
			}
		} else {
			return GamePhoController.NAME;
		}
	}

	protected void addClickAllowed(InterfaceDevice requestingDevice, JSONBuilder data) {
		if (requestingDevice instanceof Player) {
			data.nestedAttribute("playTab", "clickDisabled", requestingPlayer != requestingDevice);
		}
	}

}
