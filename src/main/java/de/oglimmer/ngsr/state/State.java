package de.oglimmer.ngsr.state;

import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.InterfaceDevice;

/**
 * 
 * 
 * @author oli
 */
public interface State {

	void updateUI();

	void updateUI(InterfaceDevice interfaceDevice);

	Game getGame();

	void initUI();

	void initUI(InterfaceDevice requestingDevice);

	void enter();

	void leave();
}