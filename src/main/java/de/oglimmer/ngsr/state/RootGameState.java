package de.oglimmer.ngsr.state;

import lombok.Data;
import lombok.EqualsAndHashCode;
import de.oglimmer.ngsr.logic.ActiveMission;
import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.Player;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;
import de.oglimmer.ngsr.web.controller.ControllerFactory;
import de.oglimmer.ngsr.web.controller.GamePhoController;
import de.oglimmer.ngsr.web.controller.GameTabController;

@Data
@EqualsAndHashCode(callSuper = false)
public class RootGameState extends AbstractState {

	public RootGameState(Game game) {
		super(game);
	}

	@Override
	protected JSONBuilder getUpdateUIPlayTabForMission(ActiveMission requestingDevice) {
		return ControllerFactory.INSTANCE.getController(GameTabController.NAME).getUpdateUIPlayTab(requestingDevice);
	}

	@Override
	protected JSONBuilder getUpdateUIPlayTabForPlayer(Player requestingDevice) {
		return ControllerFactory.INSTANCE.getController(GamePhoController.NAME).getUpdateUIPlayTab(requestingDevice);
	}
}
