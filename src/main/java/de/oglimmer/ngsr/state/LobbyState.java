package de.oglimmer.ngsr.state;

import lombok.Getter;
import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.InterfaceDevice;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;
import de.oglimmer.ngsr.web.controller.BaseController;
import de.oglimmer.ngsr.web.controller.ControllerFactory;
import de.oglimmer.ngsr.web.controller.NewGameController;

public class LobbyState extends AbstractState {

	@Getter
	private static final LobbyState instance = new LobbyState();

	@Getter
	private Game game = new Game(0, 0, 0);

	private BaseController controller = ControllerFactory.INSTANCE.getController(NewGameController.NAME);

	private LobbyState() {
		super(null);
		game.pushState(this);
	}

	@Override
	protected JSONBuilder getUpdateUIPlayTabForAnonymous(InterfaceDevice requestingDevice) {
		return controller.getUpdateUIPlayTab(requestingDevice);
	}

}
