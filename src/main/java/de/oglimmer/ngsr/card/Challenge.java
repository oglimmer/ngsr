package de.oglimmer.ngsr.card;

import java.util.HashSet;
import java.util.Set;

import lombok.Data;
import lombok.EqualsAndHashCode;
import de.oglimmer.ngsr.card.attribute.CardAttribute;
import de.oglimmer.ngsr.card.attribute.CardAttributes;
import de.oglimmer.ngsr.card.keyword.ChallengeKeywords;

@Data
@EqualsAndHashCode(callSuper = true, of = {})
abstract public class Challenge extends Card {

	private Set<CardAttributes> requiredAttributes = new HashSet<>();
	private int attack;
	private int health;
	private int armor;
	private ChallengeKeywords keywords = new ChallengeKeywords();
	private String text;
	private boolean unveiled;

	public String getRequiredAttributesHtml() {
		StringBuilder buff = new StringBuilder();
		for (CardAttributes cas : requiredAttributes) {
			if (buff.length() > 0) {
				buff.append(" or ");
			}
			StringBuilder buff2 = new StringBuilder();
			for (CardAttribute ca : cas.getAttr()) {
				if (buff2.length() > 0) {
					buff2.append(", ");
				}
				buff2.append(ca.getType());
				buff2.append("-");
				buff2.append(ca.getLevel());
			}
			buff.append(buff2);
		}
		return buff.toString();
	}

}
