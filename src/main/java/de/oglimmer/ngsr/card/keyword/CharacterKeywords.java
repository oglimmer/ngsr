package de.oglimmer.ngsr.card.keyword;

import java.util.HashSet;
import java.util.Set;

import lombok.Data;
import de.oglimmer.ngsr.web.atomsphere.JSONArrayBuilder;

@Data
public class CharacterKeywords {

	private Set<CharacterKeyword> keywords = new HashSet<>();

	public CharacterKeywords add(CharacterKeyword kw) {
		keywords.add(kw);
		return this;
	}

	public JSONArrayBuilder getJSON() {
		JSONArrayBuilder arr = JSONArrayBuilder.newArray();
		for (CharacterKeyword key : keywords) {
			arr.add(key.getJSON());
		}
		return arr;
	}

}
