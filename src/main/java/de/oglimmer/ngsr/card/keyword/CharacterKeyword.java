package de.oglimmer.ngsr.card.keyword;

import de.oglimmer.ngsr.util.StringUtil;


public enum CharacterKeyword {

	ANTI_SOCIAL, BIOTECH, FAME, GUARD, HERMIT, RECON, STAMINA, PRIME_RUNNERS;

	public String getJSON() {
		return StringUtil.makeReadable(toString());
	}

}
