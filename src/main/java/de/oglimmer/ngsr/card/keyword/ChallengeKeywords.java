package de.oglimmer.ngsr.card.keyword;

import java.util.HashSet;
import java.util.Set;

import lombok.Data;

@Data
public class ChallengeKeywords {

	private Set<ChallengeKeyword> keywords = new HashSet<>();

	public ChallengeKeywords add(ChallengeKeyword kw) {
		keywords.add(kw);
		return this;
	}

	public String getSimpleHtml() {
		StringBuilder buff = new StringBuilder();
		for (ChallengeKeyword key : keywords) {
			if (buff.length() > 0) {
				buff.append(", ");
			}
			buff.append(key.toString());
		}
		return buff.toString();
	}

	public boolean isEmpty() {
		return keywords.isEmpty();
	}

}
