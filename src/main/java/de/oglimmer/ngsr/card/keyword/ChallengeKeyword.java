package de.oglimmer.ngsr.card.keyword;

import de.oglimmer.ngsr.util.StringUtil;

public enum ChallengeKeyword {

	PERSONNEL, STREET, VEHICLE, OUTDOOR, MISCELLANEOUS, AWAKENED, ELECTRICAL, BARRIER, INDOOR;

	public String getJSON() {
		return StringUtil.makeReadable(toString());
	}

}
