package de.oglimmer.ngsr.card;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true, of = {})
abstract public class Location extends Card {

}
