package de.oglimmer.ngsr.card;

import de.oglimmer.ngsr.util.StringUtil;

public enum Profession {

	RIGGER, DETECTIVE, STREET_SAMURAI, DECKER, MERCENARY, GANGER, STREET_MAGE, COMBAT_MAGE, ROCKER;

	public String getJSON() {
		return StringUtil.makeReadable(toString());
	}

}
