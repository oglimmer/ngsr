package de.oglimmer.ngsr.card.objective;

import static de.oglimmer.ngsr.card.keyword.ChallengeKeyword.INDOOR;
import de.oglimmer.ngsr.card.Objective;

public class Assassination extends Objective {

	public Assassination() {
		setTitle("Assassination");
		setReputationAward(25);
		getExcludedKeywords().add(INDOOR);
		setBonus("¥1 for every point of Firearms possessed by surviving Runner team.");
	}
}
