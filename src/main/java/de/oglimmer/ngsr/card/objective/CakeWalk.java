package de.oglimmer.ngsr.card.objective;

import de.oglimmer.ngsr.card.Objective;

public class CakeWalk extends Objective {

	public CakeWalk() {
		setTitle("Cermak Blast");
		setReputationAward(25);
		setText("Roll D6 for each Challenge and face result instead of Challenge. Treat all Spirits as Awakened. 1-2: Mosquito Spirit 6/6(A1); 3-4: Fly Spirit 7/7 (A2); 5-6: Wasp Spirit 8/8(A3).");
		setBonus("+5 Reputation if a Runner with Demolition is present at the end of shadowrun.");
	}
}
