package de.oglimmer.ngsr.card.objective;

import de.oglimmer.ngsr.card.Objective;

public class CermakBlast extends Objective {

	public CermakBlast() {
		setTitle("Cake Walk");
		setReputationAward(10);
		setText("Treat Challenges with more than one skill listed as a sleaze requirement as if they had one skill listed.");
	}
}
