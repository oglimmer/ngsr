package de.oglimmer.ngsr.card.objective;

import static de.oglimmer.ngsr.card.keyword.ChallengeKeyword.INDOOR;
import static de.oglimmer.ngsr.card.keyword.ChallengeKeyword.VEHICLE;
import de.oglimmer.ngsr.card.Objective;

public class AmazonianHunt extends Objective {

	public AmazonianHunt() {
		setTitle("AmazonianHunt");
		setReputationAward(40);
		getExcludedKeywords().add(INDOOR).add(VEHICLE);
		setAttack((int) (Math.random() * 6 + 5));
		setHealth((int) (Math.random() * 6 + 7));
		setArmor((int) (Math.random() * 6));
	}
}
