package de.oglimmer.ngsr.card;

import lombok.Data;
import lombok.EqualsAndHashCode;
import de.oglimmer.ngsr.card.keyword.ChallengeKeywords;

@Data
@EqualsAndHashCode(callSuper = true, of = {})
abstract public class Objective extends Card {

	private int reputationAward;
	private ChallengeKeywords excludedKeywords = new ChallengeKeywords();
	private int attack;
	private int health;
	private int armor;
	private String text;
	private String bonus;

}
