package de.oglimmer.ngsr.card;

import lombok.Data;
import lombok.EqualsAndHashCode;
import de.oglimmer.ngsr.logic.Player;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

@Data
@EqualsAndHashCode(of = "id")
abstract public class Card {

	private String id;
	private String title;
	private boolean tapped;

	public Card() {
	}

	public void deployOnTable(Player playingPlayer) {
		playingPlayer.getPlayerCards().playOnTable(this);
	}

	public JSONBuilder getJSON() {
		return JSONBuilder.newObject().attribute("id", id).attribute("title", title)
				.attribute("tapped", tapped);
	}

}
