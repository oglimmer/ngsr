package de.oglimmer.ngsr.card.gear;

public enum GearKeyword {

	BURST_FIRE, INDIRECT_FIRE, SILENCED_WEAPON

}
