package de.oglimmer.ngsr.card;

import static de.oglimmer.ngsr.web.atomsphere.JSONBuilder.newObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import de.oglimmer.ngsr.card.attribute.CardAttributes;
import de.oglimmer.ngsr.card.keyword.CharacterKeywords;
import de.oglimmer.ngsr.logic.Answer;
import de.oglimmer.ngsr.logic.Player;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

@Data
@EqualsAndHashCode(callSuper = true, of = {})
abstract public class Character extends Card {

	private CardAttributes attributes = new CardAttributes();

	private int deployCost;
	private int attack;
	private int health;
	private int armor;
	private Profession profession;
	private Race race;
	private CharacterKeywords keywords = new CharacterKeywords();

	@Override
	public void deployOnTable(Player playingPlayer) {
		if (playingPlayer.getCash() < deployCost) {
			Answer.INSTANCE.get(playingPlayer).addToArrayAttribute("msg",
					newObject().attribute("text", "Not enough ¥").attribute("level", "error"));
		} else {
			playingPlayer.payCash(deployCost);
			playingPlayer.getPlayerCards().playOnTable(this);
		}
	}

	public JSONBuilder getJSON() {
		return super.getJSON().attribute("deployCost", deployCost).attribute("attack", attack)
				.attribute("health", health).attribute("armor", armor).attribute("profession", profession.getJSON())
				.attribute("race", race.getJSON()).attribute("keywords", keywords.getJSON())
				.attribute("clazz", "character");
	}
}
