package de.oglimmer.ngsr.card.attribute;

import lombok.AllArgsConstructor;
import lombok.Data;
import de.oglimmer.ngsr.util.StringUtil;

@Data
@AllArgsConstructor
public class CardAttribute {

	public enum Type {
		ATHLETICS, CONJURE, DECKINGMATRIX, DEMOLITIONS, FIREARMS, GUNNERY, LEADERSHIP, MELEE, PILOTING, SOCIAL, SORCERY, STEALTH, STREETWISE, TECHNICAL
	}

	private Type type;
	private int level;

	public String getJSON() {
		return StringUtil.makeReadable(toString());
	}
}
