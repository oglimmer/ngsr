package de.oglimmer.ngsr.card.attribute;

import java.util.HashSet;
import java.util.Set;

import lombok.Data;
import de.oglimmer.ngsr.card.attribute.CardAttribute.Type;

@Data
public class CardAttributes {

	private Set<CardAttribute> attr = new HashSet<>();

	public CardAttributes(Type... types) {
		for (Type type : types) {
			boolean found = false;
			for (CardAttribute ca : attr) {
				if (ca.getType() == type) {
					ca.setLevel(ca.getLevel() + 1);
					found = true;
				}
			}
			if (!found) {
				attr.add(new CardAttribute(type, 1));
			}
		}
	}

	public CardAttributes add(Type type) {
		return add(type, 1);
	}

	public CardAttributes add(Type type, int level) {
		attr.add(new CardAttribute(type, level));
		return this;
	}
}
