package de.oglimmer.ngsr.card;

public class CardNotFoundException extends NGSRException {

	private static final long serialVersionUID = 1L;

	public CardNotFoundException(String text) {
		super(text);
	}

}
