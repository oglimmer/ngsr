package de.oglimmer.ngsr.card.challenge;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.PILOTING;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.STREETWISE;
import static de.oglimmer.ngsr.card.keyword.ChallengeKeyword.OUTDOOR;
import static de.oglimmer.ngsr.card.keyword.ChallengeKeyword.PERSONNEL;
import static de.oglimmer.ngsr.card.keyword.ChallengeKeyword.VEHICLE;
import de.oglimmer.ngsr.card.Challenge;
import de.oglimmer.ngsr.card.attribute.CardAttributes;

public class AncientsTurf extends Challenge {

	public AncientsTurf() {
		setTitle("Ancients' Turf");
		getRequiredAttributes().add(new CardAttributes(PILOTING, PILOTING));
		getRequiredAttributes().add(new CardAttributes(STREETWISE, STREETWISE));
		getKeywords().add(OUTDOOR).add(VEHICLE).add(PERSONNEL);
		setAttack(7);
		setHealth(4);
		setText("");
	}
}
