package de.oglimmer.ngsr.card.challenge;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.FIREARMS;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.PILOTING;
import static de.oglimmer.ngsr.card.keyword.ChallengeKeyword.OUTDOOR;
import static de.oglimmer.ngsr.card.keyword.ChallengeKeyword.STREET;
import static de.oglimmer.ngsr.card.keyword.ChallengeKeyword.VEHICLE;
import de.oglimmer.ngsr.card.Challenge;
import de.oglimmer.ngsr.card.attribute.CardAttributes;

public class AmbushedEnRoute extends Challenge {

	public AmbushedEnRoute() {
		setTitle("Ambushed En Route");
		getRequiredAttributes().add(new CardAttributes(FIREARMS, FIREARMS, PILOTING));
		getKeywords().add(STREET).add(VEHICLE).add(OUTDOOR);
		setText("If alarm is triggered, Ambushed En Route inflicts 5 damage on Runner team. Trash after revealed.");
	}

}
