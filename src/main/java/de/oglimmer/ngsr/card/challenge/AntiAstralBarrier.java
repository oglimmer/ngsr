package de.oglimmer.ngsr.card.challenge;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.CONJURE;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.SORCERY;
import static de.oglimmer.ngsr.card.keyword.ChallengeKeyword.MISCELLANEOUS;
import de.oglimmer.ngsr.card.Challenge;
import de.oglimmer.ngsr.card.attribute.CardAttributes;

public class AntiAstralBarrier extends Challenge {

	public AntiAstralBarrier() {
		setTitle("Anti-Astral Barrier");
		getRequiredAttributes().add(new CardAttributes(CONJURE, CONJURE, SORCERY));
		getKeywords().add(MISCELLANEOUS);
		setText("If alarm is triggered, roll D6 for each Spirit present. On 4+, trash that card.");
	}
}
