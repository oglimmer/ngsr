package de.oglimmer.ngsr.card.challenge;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.LEADERSHIP;
import static de.oglimmer.ngsr.card.keyword.ChallengeKeyword.PERSONNEL;
import de.oglimmer.ngsr.card.Challenge;
import de.oglimmer.ngsr.card.attribute.CardAttributes;

public class Ambush extends Challenge {

	public Ambush() {
		setTitle("Ambush");
		getRequiredAttributes().add(new CardAttributes(LEADERSHIP));
		getKeywords().add(PERSONNEL);
		setAttack(6);
		setHealth(6);
		setText("Ambush inflicts damage first, Trash Runners killed by this damage (these Runners do not inflict damage).");
	}

}
