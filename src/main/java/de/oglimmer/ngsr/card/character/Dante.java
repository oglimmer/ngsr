package de.oglimmer.ngsr.card.character;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.FIREARMS;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.MELEE;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.Profession;
import de.oglimmer.ngsr.card.Race;

public class Dante extends Character {

	public static final String[] CARD_IDS = { "aaj" };

	public Dante(String cardId) {
		setId(cardId);
		setTitle("Domino");
		setProfession(Profession.STREET_SAMURAI);
		setRace(Race.DWARF);
		setAttack(6);
		setHealth(5);
		setDeployCost(6);
		getAttributes().add(FIREARMS, 2).add(MELEE);
	}

}
