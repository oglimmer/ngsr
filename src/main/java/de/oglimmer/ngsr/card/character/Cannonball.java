package de.oglimmer.ngsr.card.character;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.FIREARMS;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.GUNNERY;
import static de.oglimmer.ngsr.card.keyword.CharacterKeyword.STAMINA;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.Profession;
import de.oglimmer.ngsr.card.Race;

public class Cannonball extends Character {

	public static final String[] CARD_IDS = { "aae" };

	public Cannonball(String cardId) {
		setId(cardId);
		setTitle("Cannonball");
		setProfession(Profession.MERCENARY);
		setRace(Race.DWARF);
		setAttack(5);
		setHealth(6);
		setDeployCost(6);
		getAttributes().add(FIREARMS).add(GUNNERY);
		getKeywords().add(STAMINA);
	}

}
