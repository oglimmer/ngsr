package de.oglimmer.ngsr.card.character;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.GUNNERY;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.MELEE;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.Profession;
import de.oglimmer.ngsr.card.Race;

public class BamBam extends Character {

	public static final String[] CARD_IDS = { "aac" };

	public BamBam(String cardId) {
		setId(cardId);
		setTitle("Bam-Bam");
		setProfession(Profession.STREET_SAMURAI);
		setRace(Race.TROLL);
		setAttack(6);
		setHealth(8);
		setArmor(1);
		setDeployCost(8);
		getAttributes().add(GUNNERY).add(MELEE, 2);
	}

}
