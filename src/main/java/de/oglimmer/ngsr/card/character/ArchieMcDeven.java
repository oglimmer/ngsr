package de.oglimmer.ngsr.card.character;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.FIREARMS;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.SOCIAL;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.Profession;
import de.oglimmer.ngsr.card.Race;

public class ArchieMcDeven extends Character {

	public static final String[] CARD_IDS = { "aab" };

	public ArchieMcDeven(String cardId) {
		setId(cardId);
		setTitle("Archie McDeven");
		setProfession(Profession.DETECTIVE);
		setRace(Race.HUMAN);
		setAttack(4);
		setHealth(3);
		setDeployCost(4);
		getAttributes().add(FIREARMS).add(SOCIAL);
	}

}
