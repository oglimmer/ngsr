package de.oglimmer.ngsr.card.character;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.DEMOLITIONS;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.PILOTING;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.TECHNICAL;
import static de.oglimmer.ngsr.card.keyword.CharacterKeyword.STAMINA;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.Profession;
import de.oglimmer.ngsr.card.Race;

public class Ajax extends Character {

	public static final String[] CARD_IDS = { "aaa" };

	public Ajax(String cardId) {
		setId(cardId);
		setTitle("Ajax");
		setProfession(Profession.RIGGER);
		setRace(Race.HUMAN);
		setAttack(3);
		setHealth(3);
		setDeployCost(5);
		getAttributes().add(DEMOLITIONS).add(PILOTING).add(TECHNICAL);
		getKeywords().add(STAMINA);
	}

}
