package de.oglimmer.ngsr.card.character;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.DECKINGMATRIX;
import static de.oglimmer.ngsr.card.keyword.CharacterKeyword.RECON;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.Profession;
import de.oglimmer.ngsr.card.Race;

public class Caesar extends Character {

	public static final String[] CARD_IDS = { "aad" };

	public Caesar(String cardId) {
		setId(cardId);
		setTitle("Caesar");
		setProfession(Profession.DECKER);
		setRace(Race.ORK);
		setAttack(3);
		setHealth(4);
		setDeployCost(3);
		getAttributes().add(DECKINGMATRIX);
		getKeywords().add(RECON);
	}

}
