package de.oglimmer.ngsr.card.character;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.DEMOLITIONS;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.FIREARMS;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.LEADERSHIP;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.MELEE;
import static de.oglimmer.ngsr.card.keyword.CharacterKeyword.STAMINA;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.Profession;
import de.oglimmer.ngsr.card.Race;

public class Drake extends Character {

	public static final String[] CARD_IDS = { "aal" };

	public Drake(String cardId) {
		setId(cardId);
		setTitle("Drake");
		setProfession(Profession.MERCENARY);
		setRace(Race.ELF);
		setAttack(4);
		setHealth(5);
		setDeployCost(6);
		getAttributes().add(DEMOLITIONS).add(FIREARMS).add(LEADERSHIP).add(MELEE);
		getKeywords().add(STAMINA);
	}

}
