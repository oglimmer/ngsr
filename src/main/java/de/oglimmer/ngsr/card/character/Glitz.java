package de.oglimmer.ngsr.card.character;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.SOCIAL;
import static de.oglimmer.ngsr.card.keyword.CharacterKeyword.FAME;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.Profession;
import de.oglimmer.ngsr.card.Race;

public class Glitz extends Character {

	public static final String[] CARD_IDS = { "aam" };

	public Glitz(String cardId) {
		setId(cardId);
		setTitle("Glitz");
		setProfession(Profession.ROCKER);
		setRace(Race.ELF);
		setAttack(3);
		setHealth(2);
		setDeployCost(4);
		getAttributes().add(SOCIAL);
		getKeywords().add(FAME);
	}

}
