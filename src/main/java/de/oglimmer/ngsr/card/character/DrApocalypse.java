package de.oglimmer.ngsr.card.character;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.GUNNERY;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.SORCERY;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.Profession;
import de.oglimmer.ngsr.card.Race;

public class DrApocalypse extends Character {

	public static final String[] CARD_IDS = { "aak" };

	public DrApocalypse(String cardId) {
		setId(cardId);
		setTitle("Dr. Apocalypse");
		setProfession(Profession.COMBAT_MAGE);
		setRace(Race.DWARF);
		setAttack(4);
		setHealth(5);
		setDeployCost(6);
		getAttributes().add(GUNNERY).add(SORCERY);
	}

}
