package de.oglimmer.ngsr.card.character;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.DEMOLITIONS;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.FIREARMS;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.MELEE;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.Profession;
import de.oglimmer.ngsr.card.Race;

public class CherryBomb extends Character {

	public static final String[] CARD_IDS = { "aaf" };

	public CherryBomb(String cardId) {
		setId(cardId);
		setTitle("Cherry Bomb");
		setProfession(Profession.MERCENARY);
		setRace(Race.DWARF);
		setAttack(5);
		setHealth(6);
		setDeployCost(7);
		getAttributes().add(FIREARMS).add(MELEE).add(DEMOLITIONS);
	}

}
