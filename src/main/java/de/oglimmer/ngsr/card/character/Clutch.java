package de.oglimmer.ngsr.card.character;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.PILOTING;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.TECHNICAL;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.Profession;
import de.oglimmer.ngsr.card.Race;

public class Clutch extends Character {

	public static final String[] CARD_IDS = { "aag" };

	public Clutch(String cardId) {
		setId(cardId);
		setTitle("Clutch");
		setProfession(Profession.RIGGER);
		setRace(Race.DWARF);
		setAttack(3);
		setHealth(4);
		setDeployCost(4);
		getAttributes().add(PILOTING).add(TECHNICAL);
	}

}
