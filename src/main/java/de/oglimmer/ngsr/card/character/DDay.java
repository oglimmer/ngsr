package de.oglimmer.ngsr.card.character;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.GUNNERY;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.MELEE;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.Profession;
import de.oglimmer.ngsr.card.Race;

public class DDay extends Character {

	public static final String[] CARD_IDS = { "aah" };

	public DDay(String cardId) {
		setId(cardId);
		setTitle("D-Day");
		setProfession(Profession.GANGER);
		setRace(Race.DWARF);
		setAttack(4);
		setHealth(4);
		setDeployCost(5);
		getAttributes().add(GUNNERY).add(MELEE);
	}

}
