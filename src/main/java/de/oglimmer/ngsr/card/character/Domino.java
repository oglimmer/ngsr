package de.oglimmer.ngsr.card.character;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.SORCERY;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.Profession;
import de.oglimmer.ngsr.card.Race;

public class Domino extends Character {

	public static final String[] CARD_IDS = { "aai" };

	public Domino(String cardId) {
		setId(cardId);
		setTitle("Dante");
		setProfession(Profession.STREET_MAGE);
		setRace(Race.DWARF);
		setAttack(3);
		setHealth(4);
		setDeployCost(3);
		getAttributes().add(SORCERY, 2);
	}

}
