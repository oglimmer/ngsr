package de.oglimmer.ngsr.card.character;

import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.FIREARMS;
import static de.oglimmer.ngsr.card.attribute.CardAttribute.Type.MELEE;
import static de.oglimmer.ngsr.card.keyword.CharacterKeyword.ANTI_SOCIAL;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.Profession;
import de.oglimmer.ngsr.card.Race;

public class GoreTusk extends Character {

	public static final String[] CARD_IDS = { "aan" };

	public GoreTusk(String cardId) {
		setId(cardId);
		setTitle("Gore-Tusk");
		setProfession(Profession.STREET_SAMURAI);
		setRace(Race.TROLL);
		setAttack(7);
		setHealth(8);
		setDeployCost(8);
		setArmor(1);
		getAttributes().add(FIREARMS, 2).add(MELEE);
		getKeywords().add(ANTI_SOCIAL);
	}

}
