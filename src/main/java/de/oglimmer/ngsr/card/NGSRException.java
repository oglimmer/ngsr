package de.oglimmer.ngsr.card;

public class NGSRException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NGSRException(String message) {
		super(message);
	}

}
