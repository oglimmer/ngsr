package de.oglimmer.ngsr.card;

import de.oglimmer.ngsr.util.StringUtil;

public enum Race {

	HUMAN, TROLL, ORK, DWARF, ELF;

	public String getJSON() {
		return StringUtil.makeReadable(toString());
	}

}
