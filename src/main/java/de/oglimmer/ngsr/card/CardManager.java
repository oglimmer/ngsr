package de.oglimmer.ngsr.card;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import com.google.common.reflect.ClassPath.ClassInfo;

public enum CardManager {
	INSTANCE;

	private CharacterBuilder charBuilder = new CharacterBuilder();
	private NoArgConstructorBuilder<Objective> objectiveBuilder = new NoArgConstructorBuilder<>(Objective.class);
	private NoArgConstructorBuilder<Challenge> challengeBuilder = new NoArgConstructorBuilder<>(Challenge.class);

	public Card getCard(String id) {
		try {
			return charBuilder.getCardFromId(id);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException
				| InstantiationException | InvocationTargetException | NoSuchMethodException | IOException
				| ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public Objective getObjective() {
		try {
			return objectiveBuilder.getRandomCard();
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException
				| InstantiationException | InvocationTargetException | NoSuchMethodException | IOException
				| ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public Challenge getChallenge() {
		try {
			return challengeBuilder.getRandomCard();
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException
				| InstantiationException | InvocationTargetException | NoSuchMethodException | IOException
				| ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	static class CharacterBuilder extends PackageInspector<Card> {

		public CharacterBuilder() {
			super("de.oglimmer.ngsr.card.character", Card.class);
		}

		private Card getCardFromId(String id) throws IOException, ClassNotFoundException, NoSuchFieldException,
				IllegalAccessException, NoSuchMethodException, InstantiationException, InvocationTargetException {
			Card baseCard = getCardMaybeNull(id);
			if (baseCard == null) {
				throw new CardNotFoundException("Card " + id + " not found!");
			}
			return baseCard;
		}

		private Card getCardMaybeNull(String id) throws IOException, ClassNotFoundException, NoSuchFieldException,
				IllegalAccessException, NoSuchMethodException, InstantiationException, InvocationTargetException {

			ImmutableSet<ClassInfo> classInfoSet = getAllCardClasses();
			for (ClassInfo classInfo : classInfoSet) {
				Class<?> classInfoClass = getClassFromClassInfo(classInfo);
				if (!isAbstract(classInfoClass) && Card.class.isAssignableFrom(classInfoClass)) {
					Class<? extends Card> clazzBaseCard = getClassBaseCardFromClassInfo(classInfo);
					String[] ids = getCardIdsFromClass(clazzBaseCard);
					if (ids != null && ArrayUtils.contains(ids, id)) {
						return newInstance(id, clazzBaseCard);

					}
				}
			}
			return null;
		}

		protected String[] getCardIdsFromClass(Class<? extends Card> clazz) throws IllegalAccessException {
			try {
				Field field = clazz.getField("CARD_IDS");
				return (String[]) field.get(null);
			} catch (NoSuchFieldException e) {
				return null;
			}
		}

		protected Card newInstance(String id, Class<? extends Card> clazz) throws NoSuchMethodException,
				InstantiationException, IllegalAccessException, InvocationTargetException {
			Constructor<? extends Card> constructor = clazz.getConstructor(String.class);
			return constructor.newInstance(id);
		}
	}

	static class NoArgConstructorBuilder<T> extends PackageInspector<T> {

		public NoArgConstructorBuilder(Class<T> clazz) {
			super("de.oglimmer.ngsr.card." + clazz.getSimpleName().toLowerCase(), clazz);
		}

		private T getRandomCard() throws IOException, ClassNotFoundException, NoSuchFieldException,
				IllegalAccessException, NoSuchMethodException, InstantiationException, InvocationTargetException {

			List<Class<? extends T>> list = new ArrayList<>();
			ImmutableSet<ClassInfo> classInfoSet = getAllCardClasses();
			for (ClassInfo classInfo : classInfoSet) {
				Class<?> classInfoClass = getClassFromClassInfo(classInfo);
				if (!isAbstract(classInfoClass) && classOfT.isAssignableFrom(classInfoClass)) {
					Class<? extends T> clazzBaseCard = getClassBaseCardFromClassInfo(classInfo);
					list.add(clazzBaseCard);
				}
			}
			int randomIndex = (int) (list.size() * Math.random());
			return list.get(randomIndex).newInstance();
		}
	}

	static class PackageInspector<T> {

		private ImmutableSet<ClassInfo> classInfoCache;
		private String basePackage;
		protected Class<T> classOfT;

		public PackageInspector(String basePackage, Class<T> clazz) {
			this.basePackage = basePackage;
			this.classOfT = clazz;
		}

		protected boolean isAbstract(Class<?> clazz) {
			return Modifier.isAbstract(clazz.getModifiers());
		}

		protected synchronized ImmutableSet<ClassInfo> getAllCardClasses() throws IOException {
			if (classInfoCache == null) {
				classInfoCache = ClassPath.from(getClass().getClassLoader()).getTopLevelClassesRecursive(basePackage);
			}
			return classInfoCache;
		}

		protected Class<?> getClassFromClassInfo(ClassInfo ci) throws ClassNotFoundException {
			return Class.forName(ci.getName());
		}

		protected Class<? extends T> getClassBaseCardFromClassInfo(ClassInfo ci) throws ClassNotFoundException {
			return Class.forName(ci.getName()).asSubclass(classOfT);
		}

	}
}
