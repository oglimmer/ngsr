package de.oglimmer.ngsr.web.controller;

import static de.oglimmer.ngsr.web.atomsphere.JSONBuilder.newObject;

import java.util.Map;

import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.GameManager;
import de.oglimmer.ngsr.logic.InterfaceDevice;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

public class NewGameController extends BaseController {

	public static final String NAME = "NewGame";

	@EventHandler(buttonId = "create")
	public void create(Game game, InterfaceDevice requestingDevice, Map<String, String> map) {
		if (GameManager.INSTANCE.getGame() == null) {
			int numberPlayers = Integer.parseInt(map.get("numberPlayers"));
			int reputationToWin = Integer.parseInt(map.get("reputationToWin"));
			int numberOfObjectives = Integer.parseInt(map.get("numberOfObjectives"));
			game = GameManager.INSTANCE.createGame(numberPlayers, reputationToWin, numberOfObjectives);
			game.createActiveMissions();
		}
		game.getCurrentState().updateUI(requestingDevice);
	}

	public JSONBuilder getUpdateUIPlayTab(InterfaceDevice requestingDevice) {
		return newObject().attribute("playTab", newObject().resourceAttribute("html", "newgame.html"));
	}

}
