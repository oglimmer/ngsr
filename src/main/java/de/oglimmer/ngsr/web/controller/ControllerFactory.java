package de.oglimmer.ngsr.web.controller;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum ControllerFactory {
	INSTANCE;

	private Map<String, BaseController> instances = new HashMap<>();

	public synchronized BaseController getController(String className) {
		BaseController inst = instances.get(className);
		if (inst == null) {
			inst = createInstance(className);
			instances.put(className, inst);
		}
		return inst;
	}

	private BaseController createInstance(String className) {
		try {
			Class<? extends BaseController> clazz = Class.forName(
					"de.oglimmer.ngsr.web.controller." + className + "Controller").asSubclass(BaseController.class);
			return clazz.newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			log.error("Failed to instantiate controller : " + className, e);
			throw new RuntimeException(e);
		}
	}

}
