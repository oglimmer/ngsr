package de.oglimmer.ngsr.web.controller;

import static de.oglimmer.ngsr.web.atomsphere.JSONArrayBuilder.newArray;
import static de.oglimmer.ngsr.web.atomsphere.JSONBuilder.newObject;

import java.util.Map;

import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.InterfaceDevice;
import de.oglimmer.ngsr.logic.Player;
import de.oglimmer.ngsr.state.PlayCardState;
import de.oglimmer.ngsr.web.atomsphere.JSONArrayBuilder;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

public class GamePhoController extends BaseController {

	public static final String NAME = "GamePho";

	@EventHandler(buttonId = "nextPlayer")
	public void nextPlayer(Game game, Player player, Map<String, String> map) {
		game.nextPlayer();
	}

	@EventHandler(buttonId = "playCard")
	public void playCard(Game game, Player player, Map<String, String> map) {
		game.pushState(new PlayCardState(game, player));
		game.getCurrentState().updateUI();
	}

	@Override
	public JSONBuilder getUpdateUIPlayTab(InterfaceDevice requestingDevice) {
		if (requestingDevice instanceof Player) {
			Player player = (Player) requestingDevice;
			JSONBuilder vars = newObject().attribute("activePlayerDiv",
					"Active Player: " + player.getGame().getCurrentPlayer().getName());
			JSONBuilder html = newObject().resourceAttribute("html", "gamepho.html").attribute("vars", vars);
			JSONArrayBuilder array = newArray();
			if (player.isCurrentPlayer()) {
				JSONBuilder activePlayerDiv = newObject().attribute("id", "activePlayerDiv")
						.attribute("css", "background-color").attribute("value", "red");
				JSONBuilder nextPlayerButton = newObject().attribute("id", "GamePho_nextPlayer")
						.attribute("css", "display").attribute("value", "");
				array.add(nextPlayerButton).add(activePlayerDiv);
			}
			html.attribute("htmlAttrs", array);

			JSONBuilder base = newObject().attribute("playTab", html);
			return base;
		}
		return null;
	}
}
