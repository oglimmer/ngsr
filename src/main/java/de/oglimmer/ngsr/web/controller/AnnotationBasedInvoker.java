package de.oglimmer.ngsr.web.controller;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import de.oglimmer.ngsr.card.NGSRException;
import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.InterfaceDevice;

@Data
@AllArgsConstructor
@Slf4j
public class AnnotationBasedInvoker {

	private Game game;
	private InterfaceDevice requestingDevice;
	private Object clazz;

	public void buttonClick(String buttonId, Map<String, String> map) {
		boolean found = false;
		Method[] methods = clazz.getClass().getMethods();
		for (Method m : methods) {
			if (m.isAnnotationPresent(EventHandler.class)) {
				processAnnotatedMethodButtonClick(buttonId, map, m);
				found = true;
			}
		}
		assert found;
	}

	private void processAnnotatedMethodButtonClick(String buttonId, Map<String, String> map, Method method) {
		EventHandler eh = method.getAnnotation(EventHandler.class);
		if (eh.buttonId().equals(buttonId)) {
			invoke(map, method);
		}
	}

	private void invoke(Map<String, String> params, Method method) {
		try {
			method.invoke(clazz, game, requestingDevice, params);
		} catch (InvocationTargetException e) {
			if (e.getCause() instanceof NGSRException) {
				throw (NGSRException) e.getCause();
			}
			log.error("Failed to invoke method", e);
		} catch (IllegalAccessException | IllegalArgumentException e) {
			log.error("Failed to invoke method", e);
		}
	}

}
