package de.oglimmer.ngsr.web.controller;

import static de.oglimmer.ngsr.web.atomsphere.JSONBuilder.newObject;

import java.util.Map;

import de.oglimmer.ngsr.card.Card;
import de.oglimmer.ngsr.card.CardManager;
import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.InterfaceDevice;
import de.oglimmer.ngsr.logic.Player;
import de.oglimmer.ngsr.state.PlayCardState;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

public class PlayCardController extends BaseController {

	public static final String NAME = "PlayCard";

	@EventHandler(buttonId = "play")
	public void play(Game game, Player player, Map<String, String> map) {
		String cardId = map.get("cardId");
		Card card = CardManager.INSTANCE.getCard(cardId);
		((PlayCardState) game.getCurrentState()).setCard(card);
		game.getCurrentState().updateUI();
	}

	@EventHandler(buttonId = "cancel")
	public void cancel(Game game, InterfaceDevice requestingDevice, Map<String, String> map) {
		game.cancelState();
		game.getCurrentState().updateUI();
	}

	public JSONBuilder getUpdateUIPlayTab(InterfaceDevice requestingDevice) {
		return newObject().attribute("playTab", newObject().resourceAttribute("html", "playCard.html"));
	}

}
