package de.oglimmer.ngsr.web.controller;

import static de.oglimmer.ngsr.web.atomsphere.JSONBuilder.newObject;

import java.util.Map;

import net.sf.json.JSONObject;
import de.oglimmer.ngsr.card.NGSRException;
import de.oglimmer.ngsr.logic.Answer;
import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.InterfaceDevice;
import de.oglimmer.ngsr.logic.Player;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

/**
 * A controller gets called from the user-agent.
 * 
 * @author oli
 */
public class BaseController {

	public static final String NAME = "Base";

	/**
	 * Called to get the UI play-tab update data (like new tags, changes or modifications)
	 * 
	 * @param requestingDevice
	 * @return
	 */
	public JSONBuilder getUpdateUIPlayTab(InterfaceDevice requestingDevice) {
		return null;
	}

	public void processEvent(Game game, InterfaceDevice requestingDevice, String cmd, String cmdParam,
			Map<String, String> inputParams) {
		assert game != null;
		assert requestingDevice != null;
		assert cmd != null;
		resetMessageStores(game);
		executeEventHandler(game, requestingDevice, cmd, cmdParam, inputParams);
		flushMessageStores(game);
	}

	private void flushMessageStores(Game game) {
		for (Player player : game.getPlayers().values()) {
			JSONObject obj = player.getModel().toJSON();
			Answer.INSTANCE.get(player).attribute("model", newObject().copy(obj));
		}
		Answer.INSTANCE.send();
	}

	private void resetMessageStores(Game game) {
		game.resetAll();
		Answer.INSTANCE.reset();
	}

	private void executeEventHandler(Game game, InterfaceDevice requestingDevice, String cmd, String cmdParam,
			Map<String, String> inputParams) {
		try {
			switch (cmd) {
			case "init":
				game.getCurrentState().initUI(requestingDevice);
				break;
			case "buttonClick":
				new AnnotationBasedInvoker(game, requestingDevice, this).buttonClick(cmdParam, inputParams);
				break;
			}
		} catch (NGSRException e) {
			Answer.INSTANCE.get(requestingDevice).addToArrayAttribute("msg",
					newObject().attribute("text", e.getMessage()).attribute("level", "error"));
		} catch (Exception e) {
			Answer.INSTANCE.get(requestingDevice).addToArrayAttribute("msg",
					newObject().attribute("text", e.getMessage()).attribute("level", "fatal"));
		}
	}

}
