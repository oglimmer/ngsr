package de.oglimmer.ngsr.web.controller;

import static de.oglimmer.ngsr.web.atomsphere.JSONBuilder.newObject;

import java.util.Map;

import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.InterfaceDevice;
import de.oglimmer.ngsr.logic.Player;
import de.oglimmer.ngsr.state.WaitingForOthersToJoinState;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

public class EnterPlayernameController extends BaseController {

	public static final String NAME = "EnterPlayername";

	@EventHandler(buttonId = "select")
	public void select(Game game, Player requestingDevice, Map<String, String> map) {
		requestingDevice.setName(map.get("playerName"));
		if (!((WaitingForOthersToJoinState) game.getCurrentState()).playerAdded()) {
			game.getCurrentState().updateUI(requestingDevice);
		}
	}

	public JSONBuilder getUpdateUIPlayTab(InterfaceDevice requestingDevice) {
		return newObject().attribute("playTab", newObject().resourceAttribute("html", "enterPlayername.html"));
	}

}
