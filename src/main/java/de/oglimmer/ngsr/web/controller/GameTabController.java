package de.oglimmer.ngsr.web.controller;

import static de.oglimmer.ngsr.web.atomsphere.JSONBuilder.newObject;

import java.util.Map;

import de.oglimmer.ngsr.card.Challenge;
import de.oglimmer.ngsr.card.Objective;
import de.oglimmer.ngsr.logic.ActiveMission;
import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.InterfaceDevice;
import de.oglimmer.ngsr.state.ShadowrunState;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

public class GameTabController extends BaseController {

	public static final String NAME = "GameTab";

	@EventHandler(buttonId = "startShadowrun")
	public void startShadowrun(Game game, ActiveMission am, Map<String, String> map) {
		game.pushState(new ShadowrunState(game));
		game.getCurrentState().updateUI();
	}

	@Override
	public JSONBuilder getUpdateUIPlayTab(InterfaceDevice requestingDevice) {
		if (requestingDevice instanceof ActiveMission) {
			return getUpdateDataActiveMission((ActiveMission) requestingDevice);
		}
		return null;
	}

	private JSONBuilder getUpdateDataActiveMission(ActiveMission am) {
		JSONBuilder vars = newObject();
		addObjective(am.getObjective(), vars);
		for (int i = 0; i < 3; i++) {
			Challenge ch = am.getChallenges().get(i);
			addChallenge(vars, i, ch);
		}
		JSONBuilder html = newObject().resourceAttribute("html", "gametab.html").attribute("vars", vars);
		return newObject().attribute("playTab", html);
	}

	private void addChallenge(JSONBuilder vars, int challengeNo, Challenge ch) {
		if (ch.isUnveiled()) {
			vars.attribute("challenge" + challengeNo + "_title", ch.getTitle());
			vars.attribute("challenge" + challengeNo + "_text", ch.getText());
			if (ch.getAttack() != 0 || ch.getHealth() != 0) {
				vars.attribute("challenge" + challengeNo + "_requirement",
						ch.getAttack() + "/" + ch.getHealth() + (ch.getArmor() != 0 ? "(A" + ch.getArmor() + ")" : ""));
			}
			vars.attribute("challenge" + challengeNo + "_reqAttributes",
					"Requirements to sleaze: " + ch.getRequiredAttributesHtml());
			vars.attribute("challenge" + challengeNo + "_keywords", "Type: " + ch.getKeywords().getSimpleHtml());
		} else {
			vars.attribute("challenge" + challengeNo + "_title", "undisclosed");
		}
	}

	private void addObjective(Objective obj, JSONBuilder vars) {
		vars.attribute("missionName", obj.getTitle());
		vars.attribute("missionRep", obj.getReputationAward());
		if (!obj.getExcludedKeywords().isEmpty()) {
			vars.attribute("missionExcludedKeywords", "No " + obj.getExcludedKeywords().getSimpleHtml()
					+ " Challenges.");
		}
		vars.attribute("missionText", obj.getText());
		if (obj.getBonus() != null) {
			vars.attribute("missionBonus", "Bonus: " + obj.getBonus());
		}
		if (obj.getAttack() != 0 || obj.getHealth() != 0) {
			vars.attribute("missionRequirement", obj.getAttack() + "/" + obj.getHealth()
					+ (obj.getArmor() != 0 ? "(A" + obj.getArmor() + ")" : ""));
		}
	}
}
