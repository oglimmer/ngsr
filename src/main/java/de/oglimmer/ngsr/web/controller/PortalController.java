package de.oglimmer.ngsr.web.controller;

import static de.oglimmer.ngsr.web.atomsphere.JSONBuilder.newObject;

import java.util.Map;

import net.sf.json.JSONObject;
import de.oglimmer.ngsr.logic.AnonymousDevice;
import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.GameManager;
import de.oglimmer.ngsr.logic.InterfaceDevice;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

public class PortalController extends BaseController {

	public static final String NAME = "Portal";

	@EventHandler(buttonId = "joinGamePho")
	public void joinGamePho(Game game, InterfaceDevice requestingDevice, Map<String, String> map) {
		assert requestingDevice instanceof AnonymousDevice;
		requestingDevice = createPlayer(requestingDevice);
		game.getCurrentState().updateUI(requestingDevice);
	}

	@EventHandler(buttonId = "joinGameTab")
	public void joinGameTab(Game game, InterfaceDevice requestingDevice, Map<String, String> map) {
		assert requestingDevice instanceof AnonymousDevice;
		requestingDevice = createActiveMission(requestingDevice);
		game.getCurrentState().updateUI(requestingDevice);
	}

	@Override
	public JSONBuilder getUpdateUIPlayTab(InterfaceDevice requestingDevice) {
		return newObject().attribute("playTab", newObject().resourceAttribute("html", "portal.html"));
	}

	private InterfaceDevice createActiveMission(InterfaceDevice requestingDevice) {
		Game game = GameManager.INSTANCE.getGame();
		requestingDevice = game.getUnassignedActiveMission((AnonymousDevice) requestingDevice);
		JSONObject data = newObject().attribute("deviceId", requestingDevice.getId()).build();
		requestingDevice.getResource().getBroadcaster().broadcast(data.toString(), requestingDevice.getResource());
		return requestingDevice;
	}

	private InterfaceDevice createPlayer(InterfaceDevice requestingDevice) {
		Game game = GameManager.INSTANCE.getGame();
		InterfaceDevice player = game.createPlayer(requestingDevice);
		JSONObject data = newObject().attribute("deviceId", player.getId()).build();
		requestingDevice.getResource().getBroadcaster().broadcast(data.toString(), requestingDevice.getResource());
		return player;
	}

}
