package de.oglimmer.ngsr.web.controller;

import static de.oglimmer.ngsr.web.atomsphere.JSONArrayBuilder.newArray;
import static de.oglimmer.ngsr.web.atomsphere.JSONBuilder.newObject;

import java.util.Map;

import de.oglimmer.ngsr.card.Card;
import de.oglimmer.ngsr.card.CardManager;
import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.InterfaceDevice;
import de.oglimmer.ngsr.logic.Player;
import de.oglimmer.ngsr.state.PlayCardState;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

public class PlayCardCheckController extends BaseController {

	public static final String NAME = "PlayCardCheck";

	@EventHandler(buttonId = "play")
	public void play(Game game, Player player, Map<String, String> map) {
		String cardId = map.get("cardId");
		Card card = CardManager.INSTANCE.getCard(cardId);
		card.deployOnTable(player);
		game.cancelState();
		game.getCurrentState().updateUI();
	}

	@EventHandler(buttonId = "cancel")
	public void cancel(Game game, InterfaceDevice requestingDevice, Map<String, String> map) {
		game.cancelState();
		game.getCurrentState().updateUI();
	}

	public JSONBuilder getUpdateUIPlayTab(InterfaceDevice requestingDevice) {
		Player player = (Player) requestingDevice;
		Card card = ((PlayCardState) player.getGame().getCurrentState()).getCard();
		String cardId = card.getId();

		return newObject().attribute(
				"playTab",
				newObject().resourceAttribute("html", "playCardCheck.html")
						.attribute("vars", newObject().attribute("cardId", cardId))
						.attribute("cards", newArray().add(card.getJSON())));
	}

}
