package de.oglimmer.ngsr.web.atomsphere;

import java.util.Map;

import lombok.Data;

@Data
public class DataMessage {

	/**
	 * id from an InterfaceDevice
	 */
	private String deviceId;

	/**
	 * Name of the Controller class
	 */
	private String source;

	/**
	 * currently always "buttonClick"
	 */
	private String cmd;

	/**
	 * ui element name (e.g. a button name)
	 */
	private String param;

	/**
	 * key-value for all <input> data available
	 */
	private Map<String, String> additionalParams;

}