package de.oglimmer.ngsr.web.atomsphere;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

@ToString
@RequiredArgsConstructor(staticName = "newObject")
public class JSONBuilder {

	private JSONObject jsonObj = new JSONObject();

	public JSONObject build() {
		return jsonObj;
	}

	@SneakyThrows(value = JSONException.class)
	public JSONBuilder attribute(String key, String val) {
		jsonObj.put(key, val);
		return this;
	}

	@SneakyThrows(value = JSONException.class)
	public JSONBuilder attribute(String key, int val) {
		jsonObj.put(key, val);
		return this;
	}

	@SneakyThrows(value = JSONException.class)
	public JSONBuilder attribute(String key, boolean val) {
		jsonObj.put(key, val);
		return this;
	}

	@SneakyThrows(value = JSONException.class)
	public JSONBuilder attribute(String key, JSONBuilder val) {
		if (jsonObj.has(key)) {
			System.out.println(jsonObj);
		}
		jsonObj.put(key, val.build());
		return this;
	}

	@SneakyThrows(value = JSONException.class)
	public JSONBuilder attribute(String key, JSONArrayBuilder arrayToAdd) {
		JSONArray existingArray = jsonObj.optJSONArray(key);
		JSONArray nativeArrayToAdd = arrayToAdd.build();
		if (existingArray == null) {
			jsonObj.put(key, nativeArrayToAdd);
		} else {
			for (int i = 0; i < nativeArrayToAdd.size(); i++) {
				existingArray.add(nativeArrayToAdd.get(i));
			}
		}
		return this;
	}

	@SneakyThrows(value = JSONException.class)
	public void addToArrayAttribute(String key, JSONBuilder... builders) {
		for (JSONBuilder builder : builders) {
			JSONArray array = jsonObj.optJSONArray(key);
			if (array == null) {
				array = new JSONArray();
				jsonObj.put(key, array);
			}
			array.add(builder.build());
		}
	}

	@SneakyThrows(value = JSONException.class)
	public JSONBuilder resourceAttribute(String key, String resourceName) {
		jsonObj.put(key, loadResource(resourceName));
		return this;
	}

	@SneakyThrows(value = IOException.class)
	private String loadResource(String resourceName) {
		URL url = Resources.getResource(resourceName);
		return Resources.toString(url, Charsets.UTF_8);
	}

	@SneakyThrows(value = JSONException.class)
	public void copy(JSONBuilder data) {
		for (Iterator<?> it = data.jsonObj.keys(); it.hasNext();) {
			String key = (String) it.next();
			Object val = data.jsonObj.get(key);
			jsonObj.put(key, val);
		}
	}

	@SneakyThrows(value = JSONException.class)
	public void nestedAttribute(String path, String key, boolean val) {
		jsonObj.getJSONObject(path).put(key, val);
	}

	@SneakyThrows(value = JSONException.class)
	public JSONBuilder copy(JSONObject obj) {
		for (Iterator<?> it = obj.keys(); it.hasNext();) {
			String key = (String) it.next();
			Object val = obj.get(key);
			jsonObj.put(key, val);
		}
		return this;
	}
}