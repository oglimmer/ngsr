package de.oglimmer.ngsr.web.atomsphere;

import java.io.IOException;

import lombok.extern.slf4j.Slf4j;

import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.Get;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Message;
import org.atmosphere.config.service.Ready;
import org.atmosphere.config.service.Resume;
import org.atmosphere.cpr.ApplicationConfig;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;

import de.oglimmer.ngsr.logic.AnonymousDevice;
import de.oglimmer.ngsr.logic.Game;
import de.oglimmer.ngsr.logic.GameManager;
import de.oglimmer.ngsr.logic.InterfaceDevice;
import de.oglimmer.ngsr.state.LobbyState;
import de.oglimmer.ngsr.web.controller.BaseController;
import de.oglimmer.ngsr.web.controller.ControllerFactory;

@Slf4j
@ManagedService(path = "/comm", atmosphereConfig = ApplicationConfig.MAX_INACTIVE + "=3600000")
public class CommService {

	@Get
	public void init(AtmosphereResource resource) {
		resource.getResponse().setCharacterEncoding("UTF-8");
	}

	@Ready
	public void onReady(AtmosphereResource resource) {
		log.info("Browser {} connected.", resource.uuid());
	}

	@Resume
	public void onResume(AtmosphereResource resource) {
		log.info("onResume");
	}

	@Disconnect
	public void onDisconnect(AtmosphereResourceEvent event) {
		if (event.isCancelled()) {
			log.info("Browser {} unexpectedly disconnected", event.getResource().uuid());
		} else if (event.isClosedByClient()) {
			log.info("Browser {} closed the connection", event.getResource().uuid());
		}
	}

	@Message(encoders = { JacksonEncoder.class }, decoders = { JacksonDecoder.class })
	public void onMessage(AtmosphereResource resource, DataMessage message) throws IOException {
		processEvent(resource, message);
	}

	private void processEvent(AtmosphereResource resource, DataMessage message) {
		log.info("{}: {} just send {} with {} ({})", message.getDeviceId(), message.getSource(), message.getCmd(),
				message.getParam(), message.getAdditionalParams());
		InterfaceDevice requestingDevice = getRequestingDevice(resource, message);
		BaseController controller = ControllerFactory.INSTANCE.getController(message.getSource());
		controller.processEvent(getGame(), requestingDevice, message.getCmd(), message.getParam(),
				message.getAdditionalParams());
	}

	private Game getGame() {
		Game game = GameManager.INSTANCE.getGame();
		if (game == null) {
			game = LobbyState.getInstance().getGame();
		}
		return game;
	}

	private InterfaceDevice getRequestingDevice(AtmosphereResource resource, DataMessage message) {
		Game game = GameManager.INSTANCE.getGame();
		InterfaceDevice id = null;
		if (game != null) {
			id = game.getInterfaceDevice(message.getDeviceId());
			if (id != null) {
				updateResourceAfterReconnect(resource, id);
			}
		}
		if (id == null) {
			id = new AnonymousDevice(resource);
		}
		return id;
	}

	private void updateResourceAfterReconnect(AtmosphereResource resource, InterfaceDevice id) {
		if (resource != id.getResource()) {
			id.setResource(resource);
		}
	}

}