package de.oglimmer.ngsr.web.atomsphere;

import java.io.IOException;

import lombok.SneakyThrows;

import org.atmosphere.config.managed.Decoder;
import org.codehaus.jackson.map.ObjectMapper;

public class JacksonDecoder implements Decoder<String, DataMessage> {

	private final ObjectMapper mapper = new ObjectMapper();

	@SneakyThrows(value = IOException.class)
	@Override
	public DataMessage decode(String s) {
		return mapper.readValue(s, DataMessage.class);
	}
}