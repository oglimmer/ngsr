package de.oglimmer.ngsr.web.atomsphere;

import lombok.RequiredArgsConstructor;
import net.sf.json.JSONArray;

@RequiredArgsConstructor(staticName = "newArray")
public class JSONArrayBuilder {

	private JSONArray jsonObj = new JSONArray();

	public JSONArray build() {
		return jsonObj;
	}

	public JSONArrayBuilder add(JSONBuilder obj) {
		jsonObj.add(obj.build());
		return this;
	}

	public JSONArrayBuilder add(String string) {
		jsonObj.add(string);
		return this;
	}

}