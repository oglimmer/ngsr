package de.oglimmer.ngsr.web.atomsphere;

import java.io.IOException;

import lombok.SneakyThrows;

import org.atmosphere.config.managed.Encoder;
import org.codehaus.jackson.map.ObjectMapper;

public class JacksonEncoder implements Encoder<DataMessage, String> {

	private final ObjectMapper mapper = new ObjectMapper();

	@SneakyThrows(value = IOException.class)
	@Override
	public String encode(DataMessage m) {
		return mapper.writeValueAsString(m);
	}
}