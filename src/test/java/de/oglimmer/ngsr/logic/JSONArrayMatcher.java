package de.oglimmer.ngsr.logic;

import lombok.AllArgsConstructor;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

@AllArgsConstructor
public class JSONArrayMatcher extends BaseMatcher<JSONArray> {

	private String idToMatch;

	public static Matcher<? super JSONArray> hasItemWithId(String id) {
		return new JSONArrayMatcher(id);
	}

	@Override
	public boolean matches(Object item) {
		if (item instanceof JSONArray) {
			JSONArray itemAsArray = (JSONArray) item;
			for (Object itemElement : itemAsArray) {
				if (itemElement instanceof JSONObject) {
					JSONObject itemElementAsJSON = (JSONObject) itemElement;
					if (idToMatch.equals(itemElementAsJSON.optString("id"))) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("Array of JSONObjects with an attribute id = " + idToMatch);
	}
}