package de.oglimmer.ngsr.logic;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItem;

import java.util.Collection;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import org.junit.Test;

import de.oglimmer.ngsr.card.Card;
import de.oglimmer.ngsr.card.CardManager;
import de.oglimmer.ngsr.card.Character;
import de.oglimmer.ngsr.card.keyword.CharacterKeyword;
import de.oglimmer.ngsr.web.atomsphere.JSONArrayBuilder;
import de.oglimmer.ngsr.web.atomsphere.JSONBuilder;

public class JSONDeltaTest {

	@Test
	public void fullUpdate() throws JSONException {
		Player player = new Player(null, null);

		JSONBuilder base = player.getJSON();

		player.setCash(100);
		player.setReputation(200);
		Card cardAAA = CardManager.INSTANCE.getCard("aaa");
		Card cardAAB = CardManager.INSTANCE.getCard("aab");
		player.getPlayerCards().getCards().add(cardAAA);
		player.getPlayerCards().getCards().add(cardAAB);

		JSONBuilder latest = player.getJSON();
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(3));
		assertThat(result.getInt("cash"), is(100));
		assertThat(result.getInt("reputation"), is(200));
		assertThat(result.getJSONArray("cards").size(), is(2));
		assertThat(result.getJSONArray("cards"), JSONArrayMatcher.hasItemWithId("aaa"));
		assertThat(result.getJSONArray("cards"), JSONArrayMatcher.hasItemWithId("aab"));
	}

	@Test
	public void cardChangedHealth() throws JSONException {
		Player player = new Player(null, null);

		Card cardAAA = CardManager.INSTANCE.getCard("aaa");
		player.getPlayerCards().getCards().add(cardAAA);
		JSONBuilder base = player.getJSON();

		((Character) cardAAA).setHealth(0);

		JSONBuilder latest = player.getJSON();
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(1));
		assertThat(result.getJSONArray("cards").size(), is(1));
		assertThat(result.getJSONArray("cards"), JSONArrayMatcher.hasItemWithId("aaa"));
	}

	@Test
	public void cardKeywordAdded() throws JSONException {
		Player player = new Player(null, null);

		Card cardAAA = CardManager.INSTANCE.getCard("aaa");
		Card cardAAB = CardManager.INSTANCE.getCard("aab");
		player.getPlayerCards().getCards().add(cardAAA);
		player.getPlayerCards().getCards().add(cardAAB);
		JSONBuilder base = player.getJSON();

		((Character) cardAAA).getKeywords().add(CharacterKeyword.GUARD);

		JSONBuilder latest = player.getJSON();
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(1));
		assertThat(result.getJSONArray("cards").size(), is(1));
		assertThat(result.getJSONArray("cards"), JSONArrayMatcher.hasItemWithId("aaa"));
		assertThat(result.getJSONArray("cards").getJSONObject(0).getJSONArray("keywords").size(), is(2));
		assertThat((Collection<String>) result.getJSONArray("cards").getJSONObject(0).getJSONArray("keywords"),
				hasItem("Stamina"));
		assertThat((Collection<String>) result.getJSONArray("cards").getJSONObject(0).getJSONArray("keywords"),
				hasItem("Guard"));
	}

	@Test
	public void cardKeywordDeleted() throws JSONException {
		Player player = new Player(null, null);

		Card cardAAA = CardManager.INSTANCE.getCard("aaa");
		Card cardAAB = CardManager.INSTANCE.getCard("aab");
		player.getPlayerCards().getCards().add(cardAAA);
		player.getPlayerCards().getCards().add(cardAAB);
		JSONBuilder base = player.getJSON();

		((Character) cardAAA).getKeywords().getKeywords().clear();

		JSONBuilder latest = player.getJSON();
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(1));
		assertThat(result.getJSONArray("cards").size(), is(1));
		assertThat(result.getJSONArray("cards"), JSONArrayMatcher.hasItemWithId("aaa"));
		assertThat(result.getJSONArray("cards").getJSONObject(0).getJSONArray("keywords").size(), is(0));
	}

	@Test
	public void cardKeywordChanged() throws JSONException {
		Player player = new Player(null, null);

		Card cardAAA = CardManager.INSTANCE.getCard("aaa");
		Card cardAAB = CardManager.INSTANCE.getCard("aab");
		player.getPlayerCards().getCards().add(cardAAA);
		player.getPlayerCards().getCards().add(cardAAB);
		JSONBuilder base = player.getJSON();

		((Character) cardAAA).getKeywords().getKeywords().clear();
		((Character) cardAAA).getKeywords().add(CharacterKeyword.ANTI_SOCIAL);

		JSONBuilder latest = player.getJSON();
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(1));
		assertThat(result.getJSONArray("cards").size(), is(1));
		assertThat(result.getJSONArray("cards"), JSONArrayMatcher.hasItemWithId("aaa"));
		assertThat(result.getJSONArray("cards").getJSONObject(0).getJSONArray("keywords").size(), is(1));
		assertThat((Collection<String>) result.getJSONArray("cards").getJSONObject(0).getJSONArray("keywords"),
				hasItem("Anti-Social"));
	}

	// 1-object n-{ string:plain }

	@Test
	public void primitiveEqual() {
		JSONBuilder base = JSONBuilder.newObject().attribute("attr1", "test");
		JSONBuilder latest = JSONBuilder.newObject().attribute("attr1", "test");
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(0));
	}

	@Test
	public void primitiveManyEqual() {
		JSONBuilder base = JSONBuilder.newObject().attribute("attr1", "test").attribute("attr2", 5)
				.attribute("attr3", true);
		JSONBuilder latest = JSONBuilder.newObject().attribute("attr1", "test").attribute("attr2", 5)
				.attribute("attr3", true);
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(0));
	}

	@Test
	public void primitiveDelta1() throws JSONException {
		JSONBuilder base = JSONBuilder.newObject().attribute("attr1", "test");
		JSONBuilder latest = JSONBuilder.newObject().attribute("attr1", "test2");
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(1));
		assertThat(result.getString("attr1"), is("test2"));
	}

	@Test
	public void primitiveDelta2() throws JSONException {
		JSONBuilder base = JSONBuilder.newObject().attribute("attr1", "test").attribute("attr2", 5)
				.attribute("attr3", true);
		JSONBuilder latest = JSONBuilder.newObject().attribute("attr1", "test2").attribute("attr2", 15)
				.attribute("attr3", false);
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(3));
		assertThat(result.getString("attr1"), is("test2"));
		assertThat(result.getInt("attr2"), is(15));
		assertThat(result.getBoolean("attr3"), is(false));
	}

	// 1-object n-{ string:object n-{string:plain } }

	@Test
	public void nestedObjectEqual() {
		JSONBuilder base = JSONBuilder.newObject().attribute("attr1",
				JSONBuilder.newObject().attribute("nested1", "test"));
		JSONBuilder latest = JSONBuilder.newObject().attribute("attr1",
				JSONBuilder.newObject().attribute("nested1", "test"));
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(0));
	}

	@Test
	public void nestedObjectDelta1() throws JSONException {
		JSONBuilder base = JSONBuilder.newObject().attribute("attr1",
				JSONBuilder.newObject().attribute("nested1", "test"));
		JSONBuilder latest = JSONBuilder.newObject().attribute("attr1",
				JSONBuilder.newObject().attribute("nested1", "test2"));
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(1));
		assertThat(result.getJSONObject("attr1").getString("nested1"), is("test2"));
	}

	// 1-object n-{ string:object n-{string:object n-{string:plain } } }

	@Test
	public void nestedNestedObjectEqual() throws JSONException {
		JSONBuilder base = JSONBuilder.newObject().attribute("attr1",
				JSONBuilder.newObject().attribute("nested1", JSONBuilder.newObject().attribute("nested2", "test")));
		JSONBuilder latest = JSONBuilder.newObject().attribute("attr1",
				JSONBuilder.newObject().attribute("nested1", JSONBuilder.newObject().attribute("nested2", "test")));
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(0));
	}

	@Test
	public void nestedObjectDelta2() throws JSONException {
		JSONBuilder base = JSONBuilder.newObject().attribute("attr1",
				JSONBuilder.newObject().attribute("nested1", JSONBuilder.newObject().attribute("nested2", "test")));
		JSONBuilder latest = JSONBuilder.newObject().attribute("attr1",
				JSONBuilder.newObject().attribute("nested1", JSONBuilder.newObject().attribute("nested2", "test2")));
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(1));
		assertThat(result.getJSONObject("attr1").getJSONObject("nested1").getString("nested2"), is("test2"));
	}

	// 1-object n-{ string:array_of_object n-{string:plain } }

	@Test
	public void arrayEqual() {
		JSONBuilder base = JSONBuilder.newObject().attribute(
				"attr1",
				JSONArrayBuilder.newArray()
						.add(JSONBuilder.newObject().attribute("id", 1).attribute("arrObj1", "test")));
		JSONBuilder latest = JSONBuilder.newObject().attribute(
				"attr1",
				JSONArrayBuilder.newArray()
						.add(JSONBuilder.newObject().attribute("id", 1).attribute("arrObj1", "test")));
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(0));
	}

	@Test
	public void arrayDeltaMod1() throws JSONException {
		JSONBuilder base = JSONBuilder.newObject().attribute(
				"attr1",
				JSONArrayBuilder.newArray()
						.add(JSONBuilder.newObject().attribute("id", 1).attribute("arrObj1", "test")));
		JSONBuilder latest = JSONBuilder.newObject().attribute(
				"attr1",
				JSONArrayBuilder.newArray().add(
						JSONBuilder.newObject().attribute("id", 1).attribute("arrObj1", "test2")));
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(1));
		assertThat(result.getJSONArray("attr1").getJSONObject(0).getString("arrObj1"), is("test2"));
	}

	@Test
	public void arrayDeltaAdd1() throws JSONException {
		JSONBuilder base = JSONBuilder.newObject().attribute(
				"attr1",
				JSONArrayBuilder.newArray()
						.add(JSONBuilder.newObject().attribute("id", 1).attribute("arrObj1", "test")));
		JSONBuilder latest = JSONBuilder.newObject().attribute(
				"attr1",
				JSONArrayBuilder.newArray()
						.add(JSONBuilder.newObject().attribute("id", 1).attribute("arrObj1", "test"))
						.add(JSONBuilder.newObject().attribute("id", 2).attribute("arrObj1", "item2")));
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(1));
		assertThat(result.getJSONArray("attr1").getJSONObject(0).getString("arrObj1"), is("item2"));
	}

	@Test
	public void arrayDeltaDel1() throws JSONException {
		JSONBuilder base = JSONBuilder.newObject().attribute(
				"attr1",
				JSONArrayBuilder.newArray()
						.add(JSONBuilder.newObject().attribute("id", 1).attribute("arrObj1", "test"))
						.add(JSONBuilder.newObject().attribute("id", 2).attribute("arrObj1", "item2")));
		JSONBuilder latest = JSONBuilder.newObject().attribute(
				"attr1",
				JSONArrayBuilder.newArray()
						.add(JSONBuilder.newObject().attribute("id", 1).attribute("arrObj1", "test")));
		JSONDelta delta = new JSONDelta();
		JSONObject result = delta.calcDelta(base, latest);
		assertThat(result.size(), is(1));
		assertThat(result.getJSONArray("attr1").getJSONObject(0).getString("id"), is("2"));
		assertThat(result.getJSONArray("attr1").getJSONObject(0).getBoolean("DELETED"), is(true));
	}

}
