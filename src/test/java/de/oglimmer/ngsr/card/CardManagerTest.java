package de.oglimmer.ngsr.card;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;

import org.junit.Test;

import de.oglimmer.ngsr.card.character.Ajax;

public class CardManagerTest {

	@Test
	public void testCard() {
		Card card = CardManager.INSTANCE.getCard(Ajax.CARD_IDS[0]);
		assertThat(card, instanceOf(Ajax.class));
	}

	@Test
	public void testManyCards() {
		for (int i = 0; i < 10000; i++) {
			testCard();
		}
	}

	@Test
	public void testObjective() {
		Objective card = CardManager.INSTANCE.getObjective();
		assertThat(card, instanceOf(Objective.class));
	}

	@Test
	public void testChallenge() {
		Challenge card = CardManager.INSTANCE.getChallenge();
		assertThat(card, instanceOf(Challenge.class));
	}
}
